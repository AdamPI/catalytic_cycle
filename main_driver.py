"""
main driver
"""

import os
import sys
import time

import update_settings as us
import job_drivers as j_b
# import pattern_statistics as ps
import parse_spe_reaction_info as psri
# import trajectory as traj

if '../common' not in sys.path:
    sys.path.insert(0, '../common')
# fix pylint e1101 module has no member
# mu = __import__('utils', globals(), locals(), [], 0)
global_settings = __import__('global_settings', globals(), locals(), [], 0)

if __name__ == '__main__':
    TIME_I = time.time()

    # source file directory
    SRC_DIR = os.path.abspath(os.path.join(os.path.realpath(
        sys.argv[0]), os.pardir, os.pardir, os.pardir))
    print(SRC_DIR)
    # data directory
    DATA_DIR = os.path.abspath(os.path.join(os.path.realpath(
        sys.argv[0]), os.pardir, os.pardir, os.pardir, os.pardir, "SOHR_DATA"))
    print(DATA_DIR)

    G_S = global_settings.get_setting(DATA_DIR)
    us.update_basic_setting(DATA_DIR, G_S)

    # # run dlosde
    # j_b.run_dlsode(DATA_DIR, G_S['traj_max_t'], G_S['traj_critical_t'])

    # update terminal species
    j_b.update_terminal_species_setting(DATA_DIR, G_S['terminal_spe'])

    # update chattering species and fast reactions
    j_b.update_chattering_species_setting(
        DATA_DIR, G_S['atom_f'])

    # # quick clean up, remove a few files
    # j_b.quick_clean_up(DATA_DIR, flag="", species_path=G_S['species_path'])

    # # run monte carlo trajectory
    # j_b.run_mc_trajectory(
    #     SRC_DIR, DATA_DIR, n_traj=G_S['mc_n_traj'], atom_followed=G_S['atom_f'],
    #     init_spe=G_S['init_s'], tau=G_S['tau'], begin_t=G_S['begin_t'], end_t=G_S['mc_t'],
    #     species_path=G_S['species_path'])

    # # evaluate path integral-->pathway probability, multiple use
    # j_b.evaluate_pathway_probability(
    #     SRC_DIR, DATA_DIR, top_n=G_S['top_n_p'], num_t=G_S['pi_n_time'], flag="",
    #     n_traj=G_S['pi_n_traj'], atom_followed=G_S['atom_f'], init_spe=G_S['init_s'],
    #     traj_max_t=G_S['traj_max_t'], tau=G_S['tau'], begin_t=G_S['begin_t'], end_t=G_S['end_t'],
    #     top_n_s=G_S['top_n_s'], spe_oriented=G_S['spe_oriented'],
    #     end_s_idx=G_S['end_s_idx'], species_path=G_S['species_path'],
    #     path_reg=G_S['path_reg'], no_path_reg=G_S['no_path_reg'],
    #     path_product_spe_idx=None,
    #     fixed_t0_or_tf=G_S['fixed_t0_or_tf'],
    #     same_path_list=True)

    # T0_VEC = [0.0, 0.00257183139511, 0.00514366279022, 0.00771549418533, 0.0102873255804, 0.0128591569755, 0.0154309883707, 0.0180028197658, 0.0205746511609, 0.023146482556, 0.0257183139511, 0.0282901453462, 0.0308619767413, 0.0334338081364, 0.0360056395315, 0.0385774709266, 0.0411493023218, 0.0437211337169, 0.046292965112, 0.0488647965071, 0.0514366279022, 0.0540084592973, 0.0565802906924, 0.0591521220875, 0.0617239534826,
    #           0.0642957848777, 0.0668676162729, 0.069439447668, 0.0720112790631, 0.0745831104582, 0.0771549418533, 0.0797267732484, 0.0822986046435, 0.0848704360386, 0.0874422674337, 0.0900140988288, 0.092585930224, 0.0951577616191, 0.0977295930142, 0.100301424409, 0.102873255804, 0.1054450872, 0.108016918595, 0.11058874999, 0.113160581385, 0.11573241278, 0.118304244175, 0.12087607557, 0.123447906965, 0.12601973836, 0.128591569755]
    # DELTA_T_VEC = [0.0, 0.00257183139511, 0.00514366279022, 0.00771549418533, 0.0102873255804, 0.0128591569755, 0.0154309883707, 0.0180028197658, 0.0205746511609, 0.023146482556, 0.0257183139511, 0.0282901453462, 0.0308619767413, 0.0334338081364, 0.0360056395315, 0.0385774709266, 0.0411493023218, 0.0437211337169, 0.046292965112, 0.0488647965071, 0.0514366279022, 0.0540084592973, 0.0565802906924, 0.0591521220875, 0.0617239534826,
    #                0.0642957848777, 0.0668676162729, 0.069439447668, 0.0720112790631, 0.0745831104582, 0.0771549418533, 0.0797267732484, 0.0822986046435, 0.0848704360386, 0.0874422674337, 0.0900140988288, 0.092585930224, 0.0951577616191, 0.0977295930142, 0.100301424409, 0.102873255804, 0.1054450872, 0.108016918595, 0.11058874999, 0.113160581385, 0.11573241278, 0.118304244175, 0.12087607557, 0.123447906965, 0.12601973836, 0.128591569755]

    T0_VEC = [0.0, 0.0514363499444, 0.102872699889, 0.154309049833, 0.205745399778, 0.257181749722, 0.308618099667, 0.360054449611, 0.411490799556, 0.4629271495,
              0.514363499444, 0.565799849389, 0.617236199333, 0.668672549278, 0.720108899222, 0.771545249167, 0.822981599111, 0.874417949056, 0.925854299, 0.977290648944]
    DELTA_T_VEC = [0.0, 0.0064295784877745135, 0.012859156975549027, 0.019288735463323537, 0.025718313951098054, 0.03214789243887257, 0.038577470926647074, 0.045007049414421595, 0.05143662790219611, 0.057866206389970615, 0.06429578487774514, 0.07072536336551964, 0.07715494185329415, 0.08358452034106867, 0.09001409882884319, 0.0964436773166177, 0.10287325580439222, 0.10930283429216674, 0.11573241277994123, 0.12216199126771575, 0.12859156975549027, 0.13502114824326478, 0.14145072673103928, 0.14788030521881382, 0.1543098837065883, 0.16073946219436283, 0.16716904068213734, 0.17359861916991187, 0.18002819765768638, 0.18645777614546086, 0.1928873546332354, 0.1993169331210099, 0.20574651160878443, 0.21217609009655894, 0.21860566858433347, 0.22503524707210798, 0.23146482555988246, 0.237894404047657, 0.2443239825354315, 0.250753561023206, 0.25718313951098054, 0.2636127179987551, 0.27004229648652955, 0.2764718749743041, 0.28290145346207857, 0.2893310319498531, 0.29576061043762764, 0.3021901889254021, 0.3086197674131766, 0.3150493459009511, 0.32147892438872566, 0.3279085028765002, 0.3343380813642747, 0.3407676598520492, 0.34719723833982374, 0.3536268168275983, 0.36005639531537276, 0.3664859738031473, 0.3729155522909217, 0.37934513077869625, 0.3857747092664708, 0.3922042877542453, 0.3986338662420198, 0.40506344472979433, 0.41149302321756887, 0.41792260170534334, 0.4243521801931179, 0.4307817586808924, 0.43721133716866695, 0.4436409156564414, 0.45007049414421596, 0.45650007263199044, 0.4629296511197649, 0.46935922960753945, 0.475788808095314, 0.48221838658308847, 0.488647965070863,
                   0.49507754355863753, 0.501507122046412, 0.5079367005341866, 0.5143662790219611, 0.5207958575097356, 0.5272254359975102, 0.5336550144852846, 0.5400845929730591, 0.5465141714608336, 0.5529437499486082, 0.5593733284363827, 0.5658029069241571, 0.5722324854119317, 0.5786620638997062, 0.5850916423874807, 0.5915212208752553, 0.5979507993630298, 0.6043803778508042, 0.6108099563385788, 0.6172395348263532, 0.6236691133141278, 0.6300986918019023, 0.6365282702896768, 0.6429578487774513, 0.6493874272652258, 0.6558170057530004, 0.6622465842407749, 0.6686761627285494, 0.6751057412163239, 0.6815353197040984, 0.6879648981918729, 0.6943944766796475, 0.700824055167422, 0.7072536336551966, 0.713683212142971, 0.7201127906307455, 0.7265423691185201, 0.7329719476062946, 0.7394015260940691, 0.7458311045818434, 0.752260683069618, 0.7586902615573925, 0.765119840045167, 0.7715494185329416, 0.777978997020716, 0.7844085755084906, 0.7908381539962651, 0.7972677324840396, 0.8036973109718142, 0.8101268894595887, 0.8165564679473631, 0.8229860464351377, 0.8294156249229122, 0.8358452034106867, 0.8422747818984613, 0.8487043603862358, 0.8551339388740103, 0.8615635173617848, 0.8679930958495593, 0.8744226743373339, 0.8808522528251084, 0.8872818313128829, 0.8937114098006574, 0.9001409882884319, 0.9065705667762063, 0.9130001452639809, 0.9194297237517554, 0.9258593022395298, 0.9322888807273044, 0.9387184592150789, 0.9451480377028534, 0.951577616190628, 0.9580071946784025, 0.9644367731661769, 0.9708663516539515, 0.977295930141726, 0.9837255086295005, 0.9901550871172751, 0.9965846656050495]
    j_b.chi_value_2d_t0_tf(
        SRC_DIR, DATA_DIR, top_n=G_S['top_n_p'], flag="",
        mc_n_traj=G_S['mc_n_traj'], n_traj=G_S['pi_n_traj'],
        atom_followed=G_S['atom_f'], init_spe=G_S['init_s'], traj_max_t=G_S['traj_max_t'],
        tau=G_S['tau'], mc_end_t=G_S['mc_t'],
        t0_vec=T0_VEC,
        t0_min=None, t0_max=None, end_t=G_S['end_t'], num_t=None,
        path_reg=G_S['path_reg'], no_path_reg=G_S['no_path_reg'],
        species_path=G_S['species_path'],
        user_input_path=True,
        path_product_spe_idx=10,
        delta_t_min=None, delta_t_num=None,
        delta_t_vec=DELTA_T_VEC)

    # j_b.spe_concentration_converge_at_different_times(
    #     SRC_DIR, DATA_DIR, flag="",
    #     top_n=G_S['top_n_p'],
    #     mc_n_traj=G_S['mc_n_traj'], pi_n_traj=G_S['pi_n_traj'],
    #     traj_max_t=G_S['traj_max_t'], tau=G_S['tau'],
    #     atom_followed=G_S['atom_f'],
    #     init_spe=G_S['init_s'],
    #     path_reg=G_S['path_reg'],
    #     no_path_reg=G_S['no_path_reg'],
    #     species_path=G_S['species_path'],
    #     begin_t=0.0,
    #     mc_end_t_threshlod=0.25,
    #     end_t_vec=[0.0107159062384, 0.0214318124768, 0.0321477187152, 0.0428636249537, 0.0535795311921, 0.0642954374305, 0.0750113436689, 0.0857272499073, 0.0964431561457, 0.107159062384, 0.117874968623, 0.128590874861, 0.139306781099, 0.150022687338, 0.160738593576, 0.171454499815, 0.182170406053, 0.192886312291, 0.20360221853, 0.214318124768, 0.225034031007, 0.235749937245, 0.246465843484, 0.257181749722])

    # j_b.evaluate_pathway_AT(
    #     SRC_DIR, DATA_DIR, top_n=G_S['top_n_p'], flag="",
    #     n_traj=G_S['pi_n_traj'], atom_followed=G_S['atom_f'],
    #     traj_max_t=G_S['traj_max_t'], tau=G_S['tau'], begin_t=G_S['begin_t'], end_t=G_S['end_t'],
    #     top_n_s=G_S['top_n_s'], spe_oriented=G_S['spe_oriented'],
    #     end_s_idx=G_S['end_s_idx'], species_path=G_S['species_path'])

    # j_b.evaluate_pathway_AT_no_IT(
    #     SRC_DIR, DATA_DIR, top_n=G_S['top_n_p'], flag="",
    #     n_traj=G_S['pi_n_traj'], atom_followed=G_S['atom_f'],
    #     traj_max_t=G_S['traj_max_t'], tau=G_S['tau'], begin_t=G_S['begin_t'], end_t=G_S['end_t'],
    #     top_n_s=G_S['top_n_s'], spe_oriented=G_S['spe_oriented'],
    #     end_s_idx=G_S['end_s_idx'], species_path=G_S['species_path'])

    # j_b.evaluate_pathway_AT_with_SP(
    #     SRC_DIR, DATA_DIR, top_n=G_S['top_n_p'], flag="",
    #     n_traj=G_S['pi_n_traj'], atom_followed=G_S['atom_f'],
    #     traj_max_t=G_S['traj_max_t'], tau=G_S['tau'], begin_t=G_S['begin_t'], end_t=G_S['end_t'],
    #     top_n_s=G_S['top_n_s'], spe_oriented=G_S['spe_oriented'],
    #     end_s_idx=G_S['end_s_idx'], species_path=G_S['species_path'])

    # j_b.evaluate_passage_time_of_species(
    #     SRC_DIR, DATA_DIR, flag="", n_traj=1000000,
    #     tau=G_S['tau'], begin_t=G_S['begin_t'], end_t=G_S['end_t'],
    #     init_s_idx=G_S['init_s_idx'])

    # traj.cal_passage_time_distribution(
    #     DATA_DIR, G_S['init_s_idx'][0], G_S['tau'], t_f=G_S['end_t'], n_point=100000)

    # # convert symbolic pathway to real pathway
    # # with real species names and real reaction expression
    # j_b.symbolic_path_2_real_path(DATA_DIR, top_n=G_S['top_n_p'], flag="",
    #                               end_s_idx=None, species_path=G_S['species_path'])

    if G_S['species_path'] is False:
        psri.symbolic_path_2_real_path_pff(
            DATA_DIR, 'pathway_name_candidate.csv')
    else:
        psri.symbolic_path_2_real_path_pff(
            DATA_DIR, 'species_pathway_name_candidate.csv')

    # # copy SOHR/C++ routine files
    # j_b.copy_sohr_files(DATA_DIR, species_path=G_S['species_path'])

    # ps.parse_spe_production_along_path(
    #     DATA_DIR, top_n=G_S['top_n_p'], spe_idx=[10],
    #     init_spe=G_S['init_s'], atom_followed=G_S['atom_f'],
    #     end_t=G_S['end_t'], species_path=G_S['species_path'],
    #     axis=0, path_branching_factor=False,
    #     s_consumption=False, s_production=True)

    # # # species count
    # # j_b.species_count(DATA_DIR, top_n=G_S['top_n_p'], norm=True)

    # # # reaction count
    # # j_b.reaction_count(DATA_DIR, top_n=G_S['top_n_p'], norm=True)

    # # # initiation reaction count
    # # j_b.initiation_reaction_count(DATA_DIR, top_n=G_S['top_n_p'], norm=True)

    # # # species cycle
    # # j_b.species_cycle(DATA_DIR, top_n=G_S['top_n_p'], norm=True)

    # # # species production path
    # # j_b.species_production_path(
    # #     DATA_DIR, spe='OH', top_n=G_S['top_n_p'], norm=True)

    # # # species production reaction
    # # j_b.species_production_reaction(
    # #     DATA_DIR, spe='OH', top_n=G_S['top_n_p'], norm=True)

    # # propane make figures
    # j_b.propane_make_figures(
    #    DATA_DIR, species_path=G_S['species_path'])

    # send email
    # j_b.send_email(DATA_DIR)

    TIME_E = time.time()
    print("running time:\t" +
          str("{:.2f}".format((TIME_E - TIME_I) / 3600.0)) + " hours\n")
