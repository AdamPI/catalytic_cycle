"""
cantera related routines
"""
import os
import sys
import argparse
import time
import json
from collections import defaultdict
import itertools
from subprocess import Popen, PIPE
import numpy as np

import parse_spe_reaction_info as psri

try:
    import cantera as ct
except ImportError:
    print("CANTERA NOT AVAILABLE")


def ChemKin2Cti(data_dir):
    """
    convert ChemKin files chem.inp and therm.dat to .cti
    """
    cmd = ['python', '-m', 'cantera.ck2cti',
           '--input', os.path.join(data_dir, 'input', 'chem_EXACT.inp'),
           '--thermo', os.path.join(data_dir, 'input', 'therm_EXACT.dat'),
           '--output', os.path.join(data_dir, 'input', 'chem.cti')]
    process = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    print(stdout, stderr)
    return


def Cti2Xml(data_dir):
    """
    convert .cti to .xml file
    """
    try:
        import cantera.ctml_writer as cw
        cw.convert(os.path.join(data_dir, 'input', 'chem.cti'),
                   os.path.join(data_dir, 'input', 'chem.xml'))
    except ImportError:
        print("CANTERA NOT AVAILABLE")
    return


def ChemKin2CtiXml(data_dir):
    """
    convert ChemKin files chem.inp and therm.dat to .cti and .xml files that Cantera support
    """
    ChemKin2Cti(data_dir)
    Cti2Xml(data_dir)


def get_species_from_file(data_dir):
    """
    get species composition
    """
    all_species = ct.Species.listFromFile(
        os.path.join(data_dir, "input", "chem.xml"))
    spe_composition = defaultdict(dict)

    name_list = []

    for spe in all_species:
        name_list.append(spe.name)
        spe_composition[spe.name] = spe.composition
    return name_list, spe_composition


def get_reaction_from_file(data_dir):
    """
    get reaction composition
    https://cantera.org/documentation/docs-2.4/sphinx/html/cython/kinetics.html
    """
    all_reactions = ct.Reaction.listFromFile(
        os.path.join(data_dir, "input", "chem.xml"))
    rxn_composition = defaultdict(dict)

    rxn_name_list = []

    for rxn in all_reactions:
        rxn_name_list.append(rxn.equation)
        rxn_composition[rxn.equation] = {"id": rxn.ID,
                                         "duplicate": rxn.duplicate,
                                         "equation": rxn.equation,
                                         "orders": rxn.orders,
                                         "reactant_string": rxn.reactant_string,
                                         "reactants": rxn.reactants,
                                         "product_string": rxn.product_string,
                                         "products": rxn.products,
                                         "allow_negative_orders": rxn.allow_negative_orders,
                                         "allow_nonreactant_orders": rxn.allow_nonreactant_orders,
                                         "reversible": rxn.reversible}
    return rxn_name_list, rxn_composition


def write_spe_composition_2_file(data_dir, spe_composition, tag=None):
    """
    write to json file
    """
    if tag is not None and tag != "":
        f_n = "spe_composition" + str(tag) + ".json"
    else:
        f_n = "spe_composition.json"
    with open(os.path.join(data_dir, "input", f_n), 'w') as f_h:
        json.dump(spe_composition, f_h, indent=4, sort_keys=False)


def write_rxn_composition_2_file(data_dir, rxn_composition, tag=None):
    """
    write to json file
    """
    if tag is not None and tag != "":
        f_n = "rxn_composition" + str(tag) + ".json"
    else:
        f_n = "rxn_composition.json"
    with open(os.path.join(data_dir, "input", f_n), 'w') as f_h:
        json.dump(rxn_composition, f_h, indent=4, sort_keys=False)


def compare_ChemKin_with_cantera(data_dir):
    """
    compare_ChemKin_with_cantera of species index/name,
    reaction index/equation
    """
    # Chemkin
    spe_idx_name_d, _ = psri.parse_spe_info(data_dir)
    # cantera
    spe_name_list, _ = get_species_from_file(data_dir)

    for s_idx in spe_idx_name_d:
        A = spe_idx_name_d[s_idx]
        B = spe_name_list[int(s_idx)]
        try:
            assert(A == B), \
                "Species info Chemkin != cantera at index {}".format(s_idx)
        except AssertionError as e:
            print(e, A, B)

    rxn_idx_name_d = psri.parse_chemkin_reaction_index_name(data_dir)
    cantera_rxn_list, cantera_rxn_d = get_reaction_from_file(data_dir)
    for r_idx in rxn_idx_name_d:
        A = rxn_idx_name_d[r_idx]
        # reaction index start from 0
        B = str(cantera_rxn_list[int(r_idx)-1]).replace(
            " ", "").replace("<=>", "=>")
        C = cantera_rxn_d[cantera_rxn_list[int(r_idx)-1]]['reactants']
        D = cantera_rxn_d[cantera_rxn_list[int(r_idx)-1]]['products']
        try:
            assert(A == B), \
                "Reaction info Chemkin != cantera at index {}".format(r_idx)
        except AssertionError as e:
            r_str_list = []
            for rs in C:
                for _ in range(int(C[rs])):
                    r_str_list.append(rs)
            p_str_list = []
            for ps in D:
                for _ in range(int(D[ps])):
                    p_str_list.append(ps)
            flag = False
            r_permutation = list(itertools.permutations(r_str_list))
            p_permutation = list(itertools.permutations(p_str_list))
            for rp in r_permutation:
                for pp in p_permutation:
                    if "+".join(rp) + "=>" + "+".join(pp) == A:
                        flag = True
                        break
                if flag == True:
                    break

            if flag == False:
                print(e, A, B, C, D, r_str_list, p_str_list)
    return


def eval_2nd_order_rate_const(f_n, temp=None, pressure=1.0, buffer=None, rxn_idx=0):
    """
    f_n is path of "chem.xml" mechanism file
    temp is temperature
    buffer a dict store buffer gas and thier pressure
    rxn_idx is reaction index
    evaluate 2nd order rate costant
    """
    if buffer is None:
        buffer = dict()
    if temp is None:
        temp = [1000]

    spe = ct.Species.listFromFile(f_n)
    rxn = ct.Reaction.listFromFile(f_n)
    # print(S)
    rxn_str = []
    for r_t in rxn_idx:
        rxn_str.append(str(rxn[r_t]))
        # print(rxn[r_t])
    gas = ct.Solution(thermo='IdealGas', kinetics='GasKinetics',
                      species=spe, reactions=rxn)

    # Units are a combination of kmol, m^3 and s, convert to cm^3/molecule/s
    # 10^6/(10^3 * 6.022 * 10^23)
    # kmol_m_s_to_molecule_cm_s = 10**6/(10**3 * 6.022 * 10**23)
    kmol_m_s_to_molecule_cm_s = 1e6 / (1e3 * 6.022e23)

    result = []
    for temp_t in temp:
        gas.TPY = temp_t, pressure * ct.one_atm, buffer
        # print(gas.forward_rates_of_progress[rxn_idx])
        result.append(
            gas.forward_rate_constants[rxn_idx] * kmol_m_s_to_molecule_cm_s)
    return rxn_str, result


def beta_1000_rate_constant_w2f(data_dir, beta=None, pressure=1.0, buffer=None, rxn_idx=0):
    """
    beta = 1000/T
    T = 1000/beta
    """
    if beta is None:
        beta = [1.0]
    np.savetxt(os.path.join(data_dir, "output", "beta.csv"),
               beta, fmt="%.15f", delimiter=",")
    temp = [1000.0 / b for b in beta]
    r_n, r_c = eval_2nd_order_rate_const(os.path.join(
        data_dir, "input", "chem.xml"), temp=temp, pressure=pressure, buffer=buffer, rxn_idx=rxn_idx)
    np.savetxt(os.path.join(data_dir, "output", "rxn_name.csv"),
               r_n, fmt="%s", delimiter=",")
    np.savetxt(os.path.join(data_dir, "output", "rate_constant.csv"),
               r_c, fmt="%1.15e", delimiter=",")


if __name__ == '__main__':
    INIT_TIME = time.time()

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', default="default")
    parser.add_argument('-j', '--jobType',
                        default="ChemKin2CtiXml")

    args = parser.parse_args()
    if args.directory == "default":
        DATA_DIR = os.path.abspath(os.path.join(os.path.realpath(
            sys.argv[0]), os.pardir, os.pardir, os.pardir, os.pardir, "SOHR_DATA"))
    else:
        DATA_DIR = args.directory
        # DATA_DIR = r"D:\VS_workspace\CPlusPlus\SOHR\projects\catalytic_cycle\theory\mechanism_thermo\raghu_SR_revised_v1"
    print(DATA_DIR)

    if args.jobType == "ChemKin2CtiXml":
        ChemKin2CtiXml(DATA_DIR)
    elif args.jobType == "write_spe_composition_2_file":
        _, S_C = get_species_from_file(DATA_DIR)
        write_spe_composition_2_file(DATA_DIR, S_C, tag="")
    elif args.jobType == "write_rxn_composition_2_file":
        _, RXN_C = get_reaction_from_file(DATA_DIR)
        write_rxn_composition_2_file(DATA_DIR, RXN_C, tag="")
    elif args.jobType == "compare_ChemKin_with_cantera":
        compare_ChemKin_with_cantera(DATA_DIR)
    elif args.jobType == "eval_2nd_order_rate_const":
        TEMP = [666.66666666]
        BUFFER = {"npropyl": 1.0, "O2": 1.0, "HE": 1.0}
        RXN_IDX = [564, 565, 566, 567]
        eval_2nd_order_rate_const(os.path.join(
            DATA_DIR, "input", "chem.xml"), pressure=3.0, temp=TEMP, buffer=BUFFER, rxn_idx=RXN_IDX)
    elif args.jobType == "beta_1000_rate_constant_w2f":
        BETA = np.linspace(0.65, 2.5, num=25, endpoint=True)
        BUFFER = {"npropyl": 1.0, "O2": 1.0, "HE": 1.0}
        RXN_IDX = [548, 549, 550, 551, 552, 553]
        beta_1000_rate_constant_w2f(
            DATA_DIR, beta=BETA, pressure=3.0, buffer=BUFFER, rxn_idx=RXN_IDX)

    END_TIME = time.time()
    print("Time Elapsed:\t{:.5} seconds".format(END_TIME - INIT_TIME))
