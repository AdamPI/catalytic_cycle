%% global settings
file_dir = fullfile(fileparts(mfilename('fullpath')));

% marker
% markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.', 'none'};
markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.'};

fig_prefix = 'chi_vs_t';
atom_f = 'HA4';
spe_name = 'npropyl';
tau = 0.777660157519;
end_t = '0.9';

%% global propertities
data_entry_N = 5;
% delta_t_vec = [1.2859087486111409e-06, 0.001285908748611141, 0.01285908748611141, 0.1285908748611141, 0.3214771871527852, 0.6429543743055705];

% delta_t_value = 0.00001285908748611141;
% delta_t_value = 0.0001285908748611141;
% delta_t_value = 0.001285908748611141;
% delta_t_value = 0.009644315614583557;
% delta_t_value = 0.01285908748611141;
% delta_t_value = 0.09644315614583557;
delta_t_value = 0.1285908748611141;
% delta_t_value = 0.3214771871527852;
% delta_t_value = 0.6429543743055705;

time_cell = cell(data_entry_N, 1);
yvalue_cell = cell(data_entry_N, 1);

%% load trajectory data
sohr_dir = fullfile(fileparts(mfilename('fullpath')), '..', '..', '..', '..', '..', '..', '..', '..', '..', '..', 'SOHR_DATA');

%% import time
fn_time = fullfile(sohr_dir, 'output', 'time_dlsode_M.csv');
delimiter3 = '';
formatSpec3 = '%f%[^\n\r]';
%% Open the text file.
fileID3 = fopen(fn_time,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);

%% Close the text file.
fclose(fileID3);
time_vec = dataArray3{:, 1};
%% Clear temporary variables
clearvars fn_time delimiter formatSpec fileID dataArray ans;

%% import temperature
fn_temp = fullfile(sohr_dir, 'output', 'temperature_dlsode_M.csv');
delimiter3 = '';
formatSpec3 = '%f%[^\n\r]';
%% Open the text file.
fileID3 = fopen(fn_temp,'r');
%% Read columns of data according to format string.
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID3);
%% Allocate imported array to column variable names
temp_vec = dataArray3{:, 1};
%% Clear temporary variables
clearvars fn_temp delimiter formatSpec fileID dataArray ans;

%% import time
fn_R = fullfile(sohr_dir, 'output', 'reaction_rate_dlsode_M.csv');
delimiter3 = ',';
% For more information, see the TEXTSCAN documentation.
formatSpec3 = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID3 = fopen(fn_R,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID3);
%% Create output variable
reaction_R_mat = [dataArray3{1:end-1}];
%% Clear temporary variables
clearvars fn_R delimiter formatSpec fileID dataArray ans;

%% calculate theta first, using equation (13)
r_idx_16 = 1162 + 1;
r_idx_14 = 1080 + 1;
theta13 = reaction_R_mat(:, r_idx_16) ./ reaction_R_mat(:, r_idx_14);

%% calculate alpha using equation (28)
r_idx_3 = 736 + 1;
r_idx_4 = 738 + 1;
r_idx_20 = 90 + 1;
r_idx_23 = 44 + 1;

alpha28 = (reaction_R_mat(:, r_idx_3)) ./ (reaction_R_mat(:, r_idx_3) ...
    + reaction_R_mat(:, r_idx_4) ...
    + reaction_R_mat(:, r_idx_20) ...
    + reaction_R_mat(:, r_idx_23));
%% calculate beta using equation (29)
r_idx_12 = 1082 + 1;
r_idx_26 = 914 + 1;
r_idx_27 = 922 + 1;

beta29 = (theta13 .*  reaction_R_mat(:, r_idx_14) ) ./ (theta13 .* reaction_R_mat(:, r_idx_14) ...
    + reaction_R_mat(:, r_idx_12) ...
    + reaction_R_mat(:, r_idx_26) ...
    + reaction_R_mat(:, r_idx_27));

%% read chi value, construct data structure
n_path1 = 100;
fn_2d_f1 = fullfile(file_dir, '..', ['Merchant_f_2d_S60_HA4_0.9_100000000_10000_100', '.csv']);

delimiter1 = ',';
formatStr1 = '%f%f%f';
for i=1:n_path1
    formatStr1 = strcat(formatStr1, '%f');
end
formatStr1 = strcat(formatStr1, '%[^\n\r]');
formatSpec1 = char(formatStr1);

%% Open the text file.
fileID1 = fopen(fn_2d_f1,'r');
dataArray1 = textscan(fileID1, formatSpec1, 'Delimiter', delimiter1, 'EmptyValue', NaN,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID1);
f_mat_nR = [dataArray1{1:end-1}];

t0_nR = f_mat_nR(:, 1);
tf_nR = f_mat_nR(:, 2);
tf_nR = tf_nR - t0_nR;

% path index
offset_nR = 2;

%% read iR cycle data
n_path_iR = 100;
fn_2d_f3 = fullfile(file_dir, '..', ['Merchant_f_2d_S61_HA4_0.9_100000000_10000_100', '.csv']);

delimiter3 = ',';
formatStr3 = '%f%f%f';
for i=1:n_path_iR
    formatStr3 = strcat(formatStr3, '%f');
end
formatStr3 = strcat(formatStr3, '%[^\n\r]');
formatSpec3 = char(formatStr3);
%% Open the text file.
fileID3 = fopen(fn_2d_f3,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue', NaN,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID3);
f_mat_iR = [dataArray3{1:end-1}];
t0_iR = f_mat_iR(:, 1);
tf_iR = f_mat_iR(:, 2);
tf_iR = tf_iR - t0_iR;
% path index
offset_iR = 2;

%% data transformation
% nR primary cycle
path_idx1 = linspace(1, 3, 3);
[time_cell{1, 1}, yvalue_cell{1, 1}] = interp_time_y_vec(end_t, delta_t_value, f_mat_nR, t0_nR, tf_nR, offset_nR, path_idx1);
% nR spur cycle
path_idx2 = linspace(4, n_path1, n_path1-4+1);
[time_cell{2, 1}, yvalue_cell{2, 1}] = interp_time_y_vec(end_t, delta_t_value, f_mat_nR, t0_nR, tf_nR, offset_nR, path_idx2);
% iR cycle
path_idx3 = linspace(1, n_path_iR, n_path_iR);
[time_cell{3, 1}, yvalue_cell{3, 1}] = interp_time_y_vec(end_t, delta_t_value, f_mat_iR, t0_iR, tf_iR, offset_iR, path_idx3);
% specific path, nROOH
path_idx4 = [7,8];
[time_cell{4, 1}, yvalue_cell{4, 1}] = interp_time_y_vec(end_t, delta_t_value, f_mat_nR, t0_nR, tf_nR, offset_nR, path_idx4);
% iROOH
path_idx5 = [2,6];
[time_cell{5, 1}, yvalue_cell{5, 1}] = interp_time_y_vec(end_t, delta_t_value, f_mat_iR, t0_iR, tf_iR, offset_iR, path_idx5);

%% data clean
% check nan values
for idx=1:data_entry_N
    if isempty(yvalue_cell{idx,1})
        continue
    end
    validIndices = ~isnan(yvalue_cell{idx,1});
    yvalue_cell{idx,1} = yvalue_cell{idx, 1}(validIndices);
end
validSize = length(yvalue_cell{1,1});

% alpha beta
time_chi_vec = time_cell{1, 1}(1:validSize) .* tau;

alpha_chi_vec = ones(1, length(time_chi_vec));
for idx=1:length(time_chi_vec)
    t_tmp = time_chi_vec(idx);
    alpha_chi_vec(idx) = interp1(time_vec, alpha28, t_tmp);
end
% in case the first value is NAN
alpha_chi_vec(1) = alpha_chi_vec(2);
% three alpha beta - 1
alpha_beta_delta_n = 800;
three_alpha_beta_vec = 3 .* alpha28 .* beta29 - 1;
three_alpha_beta_vec(1) = three_alpha_beta_vec(6);
three_alpha_beta_vec(2) = three_alpha_beta_vec(6);
three_alpha_beta_vec(3) = three_alpha_beta_vec(6);
three_alpha_beta_vec(4) = three_alpha_beta_vec(6);
three_alpha_beta_vec(5) = three_alpha_beta_vec(6);

% multiply by alpha
yvalue_cell{1,1}(1:validSize) = yvalue_cell{1,1}(1:validSize) .* alpha_chi_vec;
yvalue_cell{2,1}(1:validSize) = yvalue_cell{2,1}(1:validSize) .* alpha_chi_vec;
yvalue_cell{3,1}(1:validSize) = yvalue_cell{3,1}(1:validSize) .* (1 - alpha_chi_vec);
yvalue_cell{4,1}(1:validSize) = yvalue_cell{4,1}(1:validSize) .* alpha_chi_vec;
yvalue_cell{5,1}(1:validSize) = yvalue_cell{5,1}(1:validSize) .* (1 - alpha_chi_vec);

%% plot
fig = figure();
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    
%     0    0.4470    0.7410 % 1th plot
    1   0   0 % bl
    ]; 
set(fig,'defaultAxesColorOrder',co);

xpos = [0.15 0.93];
ypos = [0.095, 0.545, 0.545, 0.99];

%##########################################################################
% Panel 1
%##########################################################################
iax = 1; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

chi_N_plot = 3;
chi_colors = [
    [1, 0, 0]
    [0.0, 0.0, 0.0]
    [0.4940, 0.1840, 0.5560]
    ];

chi_colorxn_idx = 1;
chi_markerxn_idx = 1;
chi_delta_n = 5;
chi_legend_name = cell(chi_N_plot - 1,1);
chi_legend_name{1, 1} = 'primary';
chi_legend_name{2, 1} = 'primary approx';

for idx = 1:1
    % plot chi*alpha - 1
    if idx == 1
        chi_alpha_3_vec = yvalue_cell{1,1}(1:validSize);
    end
    chi_alpha_3_vec_minus_1 = chi_alpha_3_vec - 1;

    plot(time_chi_vec, chi_alpha_3_vec_minus_1, ...
        'LineWidth', 2, ...
        'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
        'HandleVisibility','off'); hold on;
    scatter(time_chi_vec(1:chi_delta_n:end), chi_alpha_3_vec_minus_1(1:chi_delta_n:end), ...
    'LineWidth', 2, ...
    'MarkerEdgeColor', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1}, ...
    'HandleVisibility','off'); hold on;
    plot(nan, nan, 'LineWidth', 2, 'LineStyle', '-', ...
    'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

    hold on;
    chi_colorxn_idx = chi_colorxn_idx + 1;
    chi_markerxn_idx = chi_markerxn_idx + 1;
end
    
% horizontal line, at 0
plot([time_vec(1) time_vec(end)], [0.0 0.0], ...
        'LineWidth', 2, ...
        'LineStyle', '--', ...
        'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
        'HandleVisibility','off'); 
hold on;
    
chi_colorxn_idx = chi_colorxn_idx + 1;
chi_markerxn_idx = chi_markerxn_idx + 1;

plot(time_vec, three_alpha_beta_vec, ...
    'LineWidth', 2, ...
    'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'HandleVisibility','off'); hold on;
scatter(time_vec(1:alpha_beta_delta_n:end), three_alpha_beta_vec(1:alpha_beta_delta_n:end), ...
'LineWidth', 2, ...
'MarkerEdgeColor', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1}, ...
'HandleVisibility','off'); hold on;
plot(nan, nan, 'LineWidth', 2, 'LineStyle', '-', ...
'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

hold on;

%% settings
grid on;
set(gca,'GridLineStyle','--');
xlim([0, max(time_cell{1,1} * tau)]);
% ylim([0, 1.5]);

get(gca, 'XTick');
set(gca, 'FontSize', 14);

% xlabel('$t$ (seconds)', 'Interpreter','latex', 'FontSize', 24);
xticklabels([]);
yticks([-0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4]);
yticklabels({'', '-0.6', '-0.4', '-0.2', '0', '0.2', '0.4'});

ylabel('$\gamma$', 'Interpreter','latex', 'FontSize', 20);

%  legend
leg_h = legend(chi_legend_name, 'Interpreter','latex');
set(leg_h, 'FontSize', 18, 'Box', 'off');
set(leg_h, 'Location', 'South');

% vertical line
x1 = 0.4;
x2 = 0.4;
chi_alpha_3_vec_minus_1 = yvalue_cell{1,1}(1:validSize) - 1;
y1 = interp1(time_chi_vec, chi_alpha_3_vec_minus_1, x1);
y2 = interp1(time_vec, three_alpha_beta_vec, x2);

h_a1 = annotation('arrow');
% https://www.mathworks.com/help/matlab/ref/matlab.graphics.shape.arrow-properties.html
set(h_a1, 'parent', gca, ...
    'position', [x1, y1,x2 - x1,y2 - y1], ...
    'HeadLength', 10, 'HeadWidth', 5, 'HeadStyle', 'vback2', ...
    'color', 'r', ...
    'LineWidth', 1.0, ...
    'LineStyle', '-');
h_a2 = annotation('arrow');
% https://www.mathworks.com/help/matlab/ref/matlab.graphics.shape.arrow-properties.html
set(h_a2, 'parent', gca, ...
    'position', [x2, y2,x2 - x1,y1 - y2], ...
    'HeadLength', 10, 'HeadWidth', 5, 'HeadStyle', 'vback2', ...
    'color', 'r', ...
    'LineWidth', 1.0, ...
    'LineStyle', '-');

% text
a_x = gca;
t_x = a_x.XLim(1) + 0.32*(a_x.XLim(2) - a_x.XLim(1));
t_y = a_x.YLim(1) + 0.85*(a_x.YLim(2) - a_x.YLim(1));
text(t_x, t_y, 'nROO+(i)nROO \rightarrow O_2+nRO+(i)nRO', ...
    'FontSize', 13, ...
    'color', 'r');

% arrow with text
h_a3 = annotation('arrow', [0.62 0.69], [0.80 0.925], 'color', 'r');

%##########################################################################
% Panel 2
%##########################################################################
iax = 2; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

chi_N_plot = 5;
chi_colors = [
    [1, 0, 0]
    [0, 0, 1]
    [1.0000, 0.5020, 0]
%     [1.0000, 0.7490, 0]
%     [0.8500, 0.3250, 0.0980]
    [0.3010, 0.7450, 0.9330]
    [0.0, 0.0, 0.0]
    ];

chi_colorxn_idx = 1;
chi_markerxn_idx = 1;
chi_delta_n = 5;
chi_legend_name = cell(chi_N_plot - 1,1);
chi_legend_name{1, 1} = 'primary';
chi_legend_name{2, 1} = 'spur cycle 1';
chi_legend_name{3, 1} = 'iR cycle 1';
chi_legend_name{4, 1} = 'exact';

for idx = 1:4
    % plot chi*alpha - 1
    if idx == 1
        chi_alpha_3_vec = yvalue_cell{1,1}(1:validSize);
    elseif idx == 2
        chi_alpha_3_vec = yvalue_cell{1,1}(1:validSize) + yvalue_cell{4,1}(1:validSize);
    elseif idx == 3
        chi_alpha_3_vec = yvalue_cell{1,1}(1:validSize) + yvalue_cell{4,1}(1:validSize) + yvalue_cell{5,1}(1:validSize);
    elseif idx == 4
        chi_alpha_3_vec = yvalue_cell{1,1}(1:validSize) + yvalue_cell{2,1}(1:validSize) + yvalue_cell{3,1}(1:validSize);
    end
    
    chi_alpha_3_vec_minus_1 = chi_alpha_3_vec - 1;
    linestype = '-';
    color = chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :);
    marker = markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1};    

    plot(time_chi_vec, chi_alpha_3_vec_minus_1, ...
        'LineWidth', 2, ...
        'color', color, ...
        'HandleVisibility','off'); hold on;
    scatter(time_chi_vec(1:chi_delta_n:end), chi_alpha_3_vec_minus_1(1:chi_delta_n:end), ...
    'LineWidth', 2, ...
    'MarkerEdgeColor', color, ...
    'marker', marker, ...
    'HandleVisibility','off'); hold on;
    plot(nan, nan, 'LineWidth', 2, 'LineStyle', linestype, ...
    'color', color, ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

    hold on;
    chi_colorxn_idx = chi_colorxn_idx + 1;
    chi_markerxn_idx = chi_markerxn_idx + 1;
end
    
% horizontal line, at 0
plot([time_vec(1) time_vec(end)], [0.0 0.0], ...
        'LineWidth', 2, ...
        'LineStyle', '--', ...
        'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
        'HandleVisibility','off'); 
hold on;   

%% settings
grid on;
set(gca,'GridLineStyle','--');
xlim([0, max(time_cell{1,1} * tau)]);
% ylim([0, 1.5]);

xt = get(gca, 'XTick');
set(gca, 'FontSize', 14);

xlabel('$t$ (seconds)', 'Interpreter','latex', 'FontSize', 24);
ylabel('$\gamma$', 'Interpreter','latex', 'FontSize', 20);
% ylabel(y_label_str, 'FontSize', 20);

%  legend
leg_h = legend(chi_legend_name, 'Interpreter','latex');
set(leg_h, 'FontSize', 18, 'Box', 'off');
set(leg_h, 'Location', 'South');

%% figure size
x0=10;
y0=10;
width=400;
height=600;
set(gcf,'units','points','position',[x0,y0,width,height]);

%% save to file
figname = strcat(fig_prefix, '_', end_t, '_three_cycles_two_panels_', num2str(delta_t_value*tau), '_v1.png');
print(fig, fullfile(file_dir, figname), '-r200', '-dpng');

%% local function definition
function [time_v_local, y_v_local] = interp_time_y_vec(end_t_local, delta_t_value_local, f_mat_local, t0_local, tf_local, offset_local, path_idx_local)
% MYMEAN Example of a local function.
    for i = 1:length(path_idx_local)
        if i==1
            f_value_local = f_mat_local(:, offset_local + path_idx_local(i));
        else
            f_value_local = f_value_local + f_mat_local(:, offset_local + path_idx_local(i));
        end    
    end
    % construct 3d surface
    xlin_local = linspace(min(t0_local), max(t0_local), 25);
    ylin_local = linspace(min(tf_local), max(tf_local), 25);
    [X_local,Y_local] = meshgrid(xlin_local, ylin_local);
    f_local = scatteredInterpolant(t0_local, tf_local, f_value_local);
    Z_local = f_local(X_local,Y_local);
    %% update Z
    for i = 1:length(X_local)    
        for j = length(X_local) - i + 1 : length(X_local)
            Z_local(i,j) = nan;
        end
    end
    % delta
    time_v_local = X_local(1, :);
    delta_t_local = ones(1, length(time_v_local));
    delta_t_local = delta_t_local.* delta_t_value_local;
    y_v_local = f_local(time_v_local, delta_t_local);
    % check data
    for i=1:length(time_v_local)
        if time_v_local(i) + delta_t_local(i) > str2double(end_t_local)
            y_v_local(i) = nan;
        end
    end

end