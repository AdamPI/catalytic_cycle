%% global settings
sohr_dir = fullfile(fileparts(mfilename('fullpath')), '..', '..', '..', '..', '..', '..', '..', 'SOHR_DATA');
pic_dir = fullfile(fileparts(mfilename('fullpath')), 'nR_iR_path8');

% marker
% markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.', 'none'};
markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.'};

fig_prefix = 'chi_vs_t_individual_path';
atom_f = 'HA4';
tau = 0.777660157519;
end_t_sohr = '1.0';
end_t_plot = '0.9';
n_path = 119;
nR_n_path = 63;
n_path_file = n_path + 3;

%% global propertities
% delta_t_value = 0.00001285908748611141;
% delta_t_value = 0.0001285908748611141;
% delta_t_value = 0.001285908748611141;
% delta_t_value = 0.009644315614583557;
% delta_t_value = 0.01285908748611141;
% delta_t_value = 0.09644315614583557;
delta_t_value = 0.1285908748611141;
% delta_t_value = 0.2571817497222282;
% delta_t_value = 0.38577262458334227;

time_cell = cell(n_path, 1);
yvalue_cell = cell(n_path, 1);

%% import time
fn_time = fullfile(sohr_dir, 'output', 'time_dlsode_M.csv');
delimiter3 = '';
formatSpec3 = '%f%[^\n\r]';
%% Open the text file.
fileID3 = fopen(fn_time,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);

%% Close the text file.
fclose(fileID3);
time_vec = dataArray3{:, 1};
%% Clear temporary variables
clearvars fn_time delimiter formatSpec fileID dataArray ans;

%% import temperature
fn_temp = fullfile(sohr_dir, 'output', 'temperature_dlsode_M.csv');
delimiter3 = '';
formatSpec3 = '%f%[^\n\r]';
%% Open the text file.
fileID3 = fopen(fn_temp,'r');
%% Read columns of data according to format string.
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID3);
%% Allocate imported array to column variable names
temp_vec = dataArray3{:, 1};
%% Clear temporary variables
clearvars fn_temp delimiter formatSpec fileID dataArray ans;

%% import reaction rate
fn_R = fullfile(sohr_dir, 'output', 'reaction_rate_dlsode_M.csv');
delimiter3 = ',';
% For more information, see the TEXTSCAN documentation.
formatSpec3 = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% Open the text file.
fileID3 = fopen(fn_R,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID3);
%% Create output variable
reaction_chi_path_matrix = [dataArray3{1:end-1}];
%% Clear temporary variables
clearvars fn_R delimiter formatSpec fileID dataArray ans;

%% calculate theta first, using equation (13)
r_idx_16 = 1162 + 1;
r_idx_14 = 1080 + 1;
theta13 = reaction_chi_path_matrix(:, r_idx_16) ./ reaction_chi_path_matrix(:, r_idx_14);

%% calculate alpha using equation (28)
r_idx_3 = 736 + 1;
r_idx_4 = 738 + 1;
r_idx_20 = 90 + 1;
r_idx_23 = 44 + 1;

alpha28 = (reaction_chi_path_matrix(:, r_idx_3)) ./ (reaction_chi_path_matrix(:, r_idx_3) ...
    + reaction_chi_path_matrix(:, r_idx_4) ...
    + reaction_chi_path_matrix(:, r_idx_20) ...
    + reaction_chi_path_matrix(:, r_idx_23));
%% calculate beta using equation (29)
r_idx_12 = 1082 + 1;
r_idx_26 = 914 + 1;
r_idx_27 = 922 + 1;

beta29 = (theta13 .*  reaction_chi_path_matrix(:, r_idx_14) ) ./ (theta13 .* reaction_chi_path_matrix(:, r_idx_14) ...
    + reaction_chi_path_matrix(:, r_idx_12) ...
    + reaction_chi_path_matrix(:, r_idx_26) ...
    + reaction_chi_path_matrix(:, r_idx_27));

%% read chi value, construct data structure
%% read chi value
fn_2d_chi = fullfile(pic_dir, 'species_chi_value_2d.csv');

delimiter = ',';
formatStr = '';
for i=1:n_path_file
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);

%% Open the text file.
fileID = fopen(fn_2d_chi,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue', NaN,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
f_mat_chi = [dataArray{1:end-1}];

t0_chi = f_mat_chi(:, 1);
tf_chi = f_mat_chi(:, 2);
tf_chi = tf_chi - t0_chi;

% path index
offset_chi = 2;

%% data transformation, construct data
% path name
path_name_latex = cell(1, n_path);
for idx = 1:n_path
    path_name_latex{1, idx} = ['mP_{', num2str(idx), '}'];
end

% nR primary cycle
for idx=1:n_path
    path_idx = idx;
    [time_cell{idx, 1}, yvalue_cell{idx, 1}] = interp_time_y_vec(end_t_sohr, delta_t_value, f_mat_chi, t0_chi, tf_chi, offset_chi, path_idx);
end

%% data clean
% check nan values
for idx=1:n_path
    if isempty(yvalue_cell{idx,1})
        continue
    end
    validIndices = ~isnan(yvalue_cell{idx,1});
    yvalue_cell{idx,1} = yvalue_cell{idx, 1}(validIndices);
end
validSize = length(yvalue_cell{1,1});

% alpha beta
time_chi_vec = time_cell{1, 1}(1:validSize) .* tau;

alpha_chi_vec = ones(1, length(time_chi_vec));
for idx=1:length(time_chi_vec)
    t_tmp = time_chi_vec(idx);
    alpha_chi_vec(idx) = interp1(time_vec, alpha28, t_tmp);
end
% in case the first value is NAN
alpha_chi_vec(1) = alpha_chi_vec(2);
% three alpha beta - 1
alpha_beta_delta_n = 800;
three_alpha_beta_vec = 3 .* alpha28 .* beta29 - 1;
three_alpha_beta_vec(1) = three_alpha_beta_vec(6);
three_alpha_beta_vec(2) = three_alpha_beta_vec(6);
three_alpha_beta_vec(3) = three_alpha_beta_vec(6);
three_alpha_beta_vec(4) = three_alpha_beta_vec(6);
three_alpha_beta_vec(5) = three_alpha_beta_vec(6);

%% construct vector and matrix for future usage
time_vec_chi = time_cell{1, 1};
chi_path_matrix = zeros(length(yvalue_cell), length(yvalue_cell{1,1}));
for row_i=1:length(yvalue_cell)
    for col_j=1:length(yvalue_cell{row_i,1})
        chi_path_matrix(row_i, col_j) = yvalue_cell{row_i, 1}(col_j);
    end
end

% multiply by alpha
for idx=1:nR_n_path
    chi_path_matrix(idx, :) = chi_path_matrix(idx, :) .* alpha_chi_vec;
end
for idx=nR_n_path+1:n_path
    chi_path_matrix(idx, :) = chi_path_matrix(idx, :) .* (1 - alpha_chi_vec);
end

%% sort by the reaction rates around 0.5 tau, idx == 3550 for example
% sort_axis = round(0.1 * length(time_vec_chi));
sort_axis = round(0.675 * length(time_vec_chi));

% source reactions
target_array = linspace(1, n_path, n_path);
number_array = ones(1, n_path);
[B,I] = sort(chi_path_matrix(target_array, sort_axis),'descend');

old_2_new_I = ones(length(I), 1);
for idx=1:length(old_2_new_I)
    old_2_new_I(I(idx)) = idx;
end

topN_array_in_old_I = {1, 2, 3, ...
    75, ...
    7, ...
    65, ...
    35, ...
    70,71,72
    };
% known topN_array_in_old_I, calculate topN_array_in_new_I
topN_array_in_new_I = cell(1,size(topN_array_in_old_I,2));
for idx1=1:size(topN_array_in_old_I,2)
    topN_array_in_new_I{1, idx1} = ones(1, length(topN_array_in_old_I{1, idx1}));
    for idx2=1:length(topN_array_in_old_I{1, idx1})
        topN_array_in_new_I{1, idx1}(idx2) = old_2_new_I(topN_array_in_old_I{1, idx1}(idx2));
    end
end

% topN_array_in_new_I = {[1, 2, 3], ...
%     [6, 9, 12, 13, 19, 20, 21, 27, 28, 31, 32, 44, 48, 55, 62, 67, 69, 73, 74, 83, 91, 101], ...
%     [5, 16, 17, 30, 45, 64, 81], ...
%     [4, 22, 23, 42, 47, 51, 52, 65, 82, 87], ...
%     [7, 14, 15, 29, 37, 40, 41, 86], ...
%     [33, 34, 35, 98, 105, 110], ...
%     [70, 71, 72], ...
%     [59, 60, 61]};

% known topN_array_in_new_I, calculate topN_array_in_old_I
% topN_array_in_old_I = cell(1,size(topN_array_in_new_I,2));
% for idx1=1:size(topN_array_in_new_I,2)
%     topN_array_in_old_I{1, idx1} = ones(1, length(topN_array_in_new_I{1, idx1}));
%     for idx2=1:length(topN_array_in_new_I{1, idx1})
%         topN_array_in_old_I{1, idx1}(idx2) = I(topN_array_in_new_I{1, idx1}(idx2));
%     end
% end

% topN_array = {1, 2, 3, [4, 5], [8, 9], 10, 11};
% topN_array_in_new_I = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

N_plot = length(topN_array_in_new_I);

% use color map
% https://www.mathworks.com/help/matlab/ref/colormap.html
other_colors = cool(1);

stack_colors = [
    [1, 0, 0] % three reds
    [1, 0, 0]
    [1, 0, 0]
%     [1.0000, 0.5020, 0]
%     [0.4660, 0.6740, 0.1880]
%     [0.9290, 0.6940, 0.1250]
%     [0.3010, 0.7450, 0.9330]    
    [0.4660, 0.6740, 0.1880]
    [0, 0.4470, 0.7410]    
    [0.8500, 0.3250, 0.0980]
    [0.9290, 0.6940, 0.1250]
    other_colors
    other_colors
    other_colors
    ];

colors = stack_colors;

ylim_range = [0, 0.4];

%% plot
fig = figure();
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    
%     0    0.4470    0.7410 % 1th plot
    1   0   0 % bl
    ]; 
set(fig,'defaultAxesColorOrder',co);

% vertical line
% plot([time_chi_vec(sort_axis), time_chi_vec(sort_axis)], ylim_range, ...
%     '--k', 'HandleVisibility','off');
% hold on;

colorxn_idx = 1;
markerxn_idx = 1;
delta_n = 9;
primary_shift = 0;
legend_name = cell(N_plot,1);
for idx1=1:N_plot
    legend_name{idx1, 1} = ['mP_{', num2str(idx1), '}'];
    
    if idx1 <= 100
        primary_shift = mod(primary_shift + 1, 5) + 1;
        shift = primary_shift;
    else
        shift = 1;
    end    
    
    % temporary top N array --> tNa
    tNa_tmp = topN_array_in_new_I{1, idx1};
    for idx2=1:length(tNa_tmp)
        path_idx = target_array(I(tNa_tmp(idx2)));
        if idx2 == 1
            data_y = chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
%             legend_name{idx1, 1} = path_name_latex{1, path_idx};
        else
            data_y = data_y + chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
%             legend_name{idx1, 1} = [legend_name{idx1, 1}, '&', path_name_latex{1, path_idx}];
        end       
    end
    
    plot(time_chi_vec, data_y, ...
        'LineWidth', 1.5, ...
        'color', colors(mod(colorxn_idx-1, length(colors))+ 1, :), ...
        'HandleVisibility','off'); hold on;
    scatter(time_chi_vec(shift:delta_n:end), data_y(shift:delta_n:end), ...
    'LineWidth', 2, ...
    'MarkerEdgeColor', colors(mod(colorxn_idx-1, length(colors))+ 1, :), ...
    'marker', markers{1, mod(markerxn_idx-1, length(markers))+ 1}, ...
    'HandleVisibility','off'); hold on;
    plot(nan, nan, 'LineWidth', 1.5, 'LineStyle', '-', ...
    'color', colors(mod(colorxn_idx-1, length(colors))+ 1, :), ...
    'marker', markers{1, mod(markerxn_idx-1, length(markers))+ 1});
    
    hold on;
    colorxn_idx = colorxn_idx + 1;
    markerxn_idx = markerxn_idx + 1;
end

%% settings
set(gca,'GridLineStyle','--');
grid on;
xlim([0, tau*str2double(end_t_plot)]);
ylim(ylim_range);
% xlim([0, time_chi_vec(end)]);

% get(gca, 'XTick');
% set(gca, 'FontSize', 13);

xlabel('t (seconds)', 'FontSize', 20);
ylabel('$\alpha(t) \cdot \chi(t, t+\delta)$', 'Interpreter', 'Latex', 'FontSize', 20);


%% global settings
axP = get(gca,'Position');

leg_h = legend(legend_name);
% set(leg_h, 'FontSize', 12, 'Box', 'off');
% set(leg_h, 'Location', 'NorthEastOutside');

set(leg_h, 'FontSize', 12, 'Box', 'on');
set(leg_h, 'Location', 'NorthEastOutside');
% set(leg_h, 'Location', 'BestOutside');

scaleRatio = 0.88;
set(gca, 'Position', [axP(1), axP(2), axP(3)*scaleRatio, axP(4)]);

% % text
% a_x = gca;
% t_x = a_x.XLim(1) + 0.625*(a_x.XLim(2) - a_x.XLim(1));
% t_y = a_x.YLim(1) + 0.85*(a_x.YLim(2) - a_x.YLim(1));
% text(t_x, t_y, ['\delta=', sprintf('%1.1f', delta_t_value*tau), ' seconds'], ...
%     'FontSize', 14, ...
%     'color', 'k');

%% save to file
figname = strcat(fig_prefix, '_', end_t_sohr, '_', num2str(sum(cell2mat(topN_array_in_new_I))), '_', num2str(sort_axis), '_v5.png');
print(fig, fullfile(pic_dir, figname), '-r200', '-dpng');

%% local function definition
function [time_v_local, y_v_local] = interp_time_y_vec(end_t_local, delta_t_value_local, f_mat_local, t0_local, tf_local, offset_local, path_idx_local)
% MYMEAN Example of a local function.
    for i = 1:length(path_idx_local)
        if i==1
            f_value_local = f_mat_local(:, offset_local + path_idx_local(i));
        else
            f_value_local = f_value_local + f_mat_local(:, offset_local + path_idx_local(i));
        end    
    end
    % construct 3d surface
    xlin_local = linspace(min(t0_local), max(t0_local), 25);
    ylin_local = linspace(min(tf_local), max(tf_local), 25);
    [X_local,Y_local] = meshgrid(xlin_local, ylin_local);
    f_local = scatteredInterpolant(t0_local, tf_local, f_value_local);
    Z_local = f_local(X_local,Y_local);
    %% update Z
    for i = 1:length(X_local)    
        for j = length(X_local) - i + 1 : length(X_local)
            Z_local(i,j) = nan;
        end
    end
    % delta
    time_v_local = X_local(1, :);
    delta_t_local = ones(1, length(time_v_local));
    delta_t_local = delta_t_local.* delta_t_value_local;
    y_v_local = f_local(time_v_local, delta_t_local);
    % check data
    for i=1:length(time_v_local)
        if time_v_local(i) + delta_t_local(i) > str2double(end_t_local)
            y_v_local(i) = nan;
        end
    end

end