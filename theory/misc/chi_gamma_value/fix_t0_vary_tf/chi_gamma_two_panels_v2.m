%% current working directory
curr_dir = fullfile(fileparts(mfilename('fullpath')));
sohr_dir = fullfile(fileparts(mfilename('fullpath')), '..', '..', '..', '..', '..', '..', '..', 'SOHR_DATA');

%% global settings
fig_prefix = 'chi_value_two_panels';
% time in unit of tau
% t_value = [0.0, 0.128590874861, 0.257181749722, 0.385772624583, 0.514363499444, 0.642954374306, 0.771545249167, 0.900136124028];
delta_t_vec = [0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9];
% delta_t_vec = [0.0, 0.0064295784877745135, 0.012859156975549027, 0.019288735463323537, 0.025718313951098054, 0.03214789243887257, 0.038577470926647074, 0.045007049414421595, 0.05143662790219611, 0.057866206389970615, 0.06429578487774514, 0.07072536336551964, 0.07715494185329415, 0.08358452034106867, 0.09001409882884319, 0.0964436773166177, 0.10287325580439222, 0.10930283429216674, 0.11573241277994123, 0.12216199126771575, 0.12859156975549027, 0.13502114824326478, 0.14145072673103928, 0.14788030521881382, 0.1543098837065883, 0.16073946219436283, 0.16716904068213734, 0.17359861916991187, 0.18002819765768638, 0.18645777614546086, 0.1928873546332354, 0.1993169331210099, 0.20574651160878443, 0.21217609009655894, 0.21860566858433347, 0.22503524707210798, 0.23146482555988246, 0.237894404047657, 0.2443239825354315, 0.250753561023206, 0.25718313951098054, 0.2636127179987551, 0.27004229648652955, 0.2764718749743041, 0.28290145346207857, 0.2893310319498531, 0.29576061043762764, 0.3021901889254021, 0.3086197674131766, 0.3150493459009511, 0.32147892438872566, 0.3279085028765002, 0.3343380813642747, 0.3407676598520492, 0.34719723833982374, 0.3536268168275983, 0.36005639531537276, 0.3664859738031473, 0.3729155522909217, 0.37934513077869625, 0.3857747092664708, 0.3922042877542453, 0.3986338662420198, 0.40506344472979433, 0.41149302321756887, 0.41792260170534334, 0.4243521801931179, 0.4307817586808924, 0.43721133716866695, 0.4436409156564414, 0.45007049414421596, 0.45650007263199044, 0.4629296511197649, 0.46935922960753945, 0.475788808095314, 0.48221838658308847, 0.488647965070863, 0.49507754355863753, 0.501507122046412, 0.5079367005341866, 0.5143662790219611, 0.5207958575097356, 0.5272254359975102, 0.5336550144852846, 0.5400845929730591, 0.5465141714608336, 0.5529437499486082, 0.5593733284363827, 0.5658029069241571, 0.5722324854119317, 0.5786620638997062, 0.5850916423874807, 0.5915212208752553, 0.5979507993630298, 0.6043803778508042, 0.6108099563385788, 0.6172395348263532, 0.6236691133141278, 0.6300986918019023, 0.6365282702896768, 0.6429578487774513, 0.6493874272652258, 0.6558170057530004, 0.6622465842407749, 0.6686761627285494, 0.6751057412163239, 0.6815353197040984, 0.6879648981918729, 0.6943944766796475, 0.700824055167422, 0.7072536336551966, 0.713683212142971, 0.7201127906307455, 0.7265423691185201, 0.7329719476062946, 0.7394015260940691, 0.7458311045818434, 0.752260683069618, 0.7586902615573925, 0.765119840045167, 0.7715494185329416, 0.777978997020716, 0.7844085755084906, 0.7908381539962651, 0.7972677324840396, 0.8036973109718142, 0.8101268894595887, 0.8165564679473631, 0.8229860464351377, 0.8294156249229122, 0.8358452034106867, 0.8422747818984613, 0.8487043603862358, 0.8551339388740103, 0.8615635173617848, 0.8679930958495593, 0.8744226743373339, 0.8808522528251084, 0.8872818313128829, 0.8937114098006574, 0.9001409882884319, 0.9065705667762063, 0.9130001452639809, 0.9194297237517554, 0.9258593022395298, 0.9322888807273044, 0.9387184592150789, 0.9451480377028534, 0.951577616190628, 0.9580071946784025, 0.9644367731661769, 0.9708663516539515, 0.977295930141726, 0.9837255086295005, 0.9901550871172751, 0.9965846656050495];

N_S = 110;
N_R = 1230;

tau = 0.777660157519;
end_t_sohr = '1.0';
end_t_plot = '0.9';

%% read Chi f value, construct data structure, the first two number are time
% n_path = 3+100;
% n_path = 3+13;
% n_path = 3+15;
n_path = 3+119;
nR_n_path = 63;

% nR or iR specifications
pic_dir = fullfile(fileparts(mfilename('fullpath')), 'nR_iR_path8');
spe_name = [num2str(n_path-3), ' path'];
path_idx = linspace(1, n_path-3, n_path-3);
t_value = [0.0, 0.128590874861, 0.257181749722, 0.385772624583, 0.514363499444, 0.642954374306];
xlim_range = [0, tau*str2double(end_t_plot)];

% pic_dir = fullfile(fileparts(mfilename('fullpath')), 'nR_iR_path8');
% spe_name = 'primary';
% path_idx = [1, 2, 3];
% t_value = [0.0, 0.128590874861, 0.257181749722, 0.385772624583, 0.514363499444, 0.642954374306];
% xlim_range = [0, tau*0.128590874861];

% pic_dir = fullfile(fileparts(mfilename('fullpath')), 'nR_iR_path8');
% spe_name = 'spur';
% path_idx = linspace(4, nR_n_path-3, nR_n_path-3-3);
% t_value = [0.0, 0.128590874861, 0.257181749722, 0.385772624583, 0.514363499444, 0.642954374306];
% xlim_range = [0, tau*str2double(end_t_plot)];

% pic_dir = fullfile(fileparts(mfilename('fullpath')), 'nR_iR_path8');
% spe_name = 'iR';
% path_idx = linspace(nR_n_path+1, n_path-3, n_path-3-nR_n_path);
% t_value = [0.0, 0.128590874861, 0.257181749722, 0.385772624583, 0.514363499444, 0.642954374306];
% xlim_range = [0, tau*str2double(end_t_plot)];

%% number of lines
N_plot = length(t_value);
time_cell = cell(N_plot, 1);
yvalue_cell = cell(N_plot, 1);

% species renaming
% "npropyloxy", "nRO"
% "npropylooh", "nROOH"
% "npropyloo", "nROO"
% "npropyl", "nR"
% "ipropyloxy", "iRO"
% "ipropylooh", "iROOH"
% "ipropyloo", "iROO"
% "ipropyl", "iR"
% "well_1", "O2QOOH1"
% "prod_1", "OQ" + '\prime' + "OOH1"
% "frag_1", "OQ" + '\prime' + "O1"

spe_name_latex = {'HE', 'AR', 'N_2', 'H', 'H_2', 'HEG', 'HCFG', 'Halt', 'O', 'O_2', 'OH', 'H_2O', 'HO_2', 'H_2O_2', 'CO', 'CO_2', 'HCO', 'CH_2O', 'formyloxy', 'formylperoxy', 'formylooh', 'C', 'CH', 'CH_2(S)', 'CH_2', 'CH_3', 'CH_4', 'CH_3OO', 'CH_3OOH', 'CH_2OH', 'CH_3O', 'CH_3OH', 'OCH_2OOH', 'HOCH_2OO', 'HOCH_2OOH', 'HOCH_2O', 'C_2H_2', 'C_2H_3', 'C_2H_4', 'C_2H_5', 'C_2H_6', 'ketene', 'HCCO', 'ethynol', 'acetaldehyde', 'acetyl', 'vinoxy', 'acetylperoxy', 'acetyloxy', 'ethoxy', 'CH_3CH_2OO', 'CH_3CH_2OOH', 'CH_2CH_2OOH', 'ethanol', 'CH_2CH_2OH', 'CH_3CHOH', 'oxirane', 'oxiranyl', 'ethenol', 'C_3H_6', 'nR', 'iR', 'RH', 'allyl', 'propen1yl', 'propen2yl', 'acetone', 'CH_3OCH_3', 'CH_3OCH_2', 'acrolein', 'CH_2CHCO', 'CH_3CHCO', 'allyloxy', 'allyl-alcohol', 'propanal', 'propionyl', 'nRO', 'iRO', 'nROO', 'nROOH', 'iROO', 'iROOH', 'propen1ol', 'propen2ol', 'CH_3CO_3H', 'O_2C_2H_4OH', 'propoxide', 'QOOH_1', 'QOOH_2', 'QOOH_3', 'O_2QOOH_1', 'well_2', 'well_3', 'well_5', 'OQ^\primeOOH_1', 'prod_2', 'prod_3', 'prod_4', 'prod_5', 'prod_6', 'prod_7', 'OQ^\primeO_1', 'frag_3', 'frag_4', 'frag_5', 'propen1oxy', 'propen2oxy', 'glyoxal', 'vinoxylmethyl', 'formylethyl'};
reaction_name_latex = {'H+O_2 \rightarrow O+OH', 'O+OH \rightarrow H+O_2', 'O+H_2 \rightarrow H+OH', 'H+OH \rightarrow O+H_2', 'H_2+OH \rightarrow H_2O+H', 'H_2O+H \rightarrow H_2+OH', 'O+H_2O \rightarrow OH+OH', 'OH+OH \rightarrow O+H_2O', 'H_2+M \rightarrow H+H+M', 'H+H+M \rightarrow H_2+M', 'H_2+AR \rightarrow H+H+AR', 'H+H+AR \rightarrow H_2+AR', 'H_2+HE \rightarrow H+H+HE', 'H+H+HE \rightarrow H_2+HE', 'O+O+M \rightarrow O_2+M', 'O_2+M \rightarrow O+O+M', 'O+O+AR \rightarrow O_2+AR', 'O_2+AR \rightarrow O+O+AR', 'O+O+HE \rightarrow O_2+HE', 'O_2+HE \rightarrow O+O+HE', 'O+H+M \rightarrow OH+M', 'OH+M \rightarrow O+H+M', 'H+OH+M \rightarrow H_2O+M', 'H_2O+M \rightarrow H+OH+M', 'H+O_2(+M) \rightarrow HO_2(+M)', 'HO_2(+M) \rightarrow H+O_2(+M)', 'HO_2+H \rightarrow H_2+O_2', 'H_2+O_2 \rightarrow HO_2+H', 'HO_2+H \rightarrow OH+OH', 'OH+OH \rightarrow HO_2+H', 'HO_2+O \rightarrow O_2+OH', 'O_2+OH \rightarrow HO_2+O', 'HO_2+OH \rightarrow H_2O+O_2', 'H_2O+O_2 \rightarrow HO_2+OH', 'HO_2+HO_2 \rightarrow H_2O_2+O_2', 'H_2O_2+O_2 \rightarrow HO_2+HO_2', 'H_2O_2(+M) \rightarrow OH+OH(+M)', 'OH+OH(+M) \rightarrow H_2O_2(+M)', 'H_2O_2+H \rightarrow H_2O+OH', 'H_2O+OH \rightarrow H_2O_2+H', 'H_2O_2+H \rightarrow HO_2+H_2', 'HO_2+H_2 \rightarrow H_2O_2+H', 'H_2O_2+O \rightarrow OH+HO_2', 'OH+HO_2 \rightarrow H_2O_2+O', 'H_2O_2+OH \rightarrow HO_2+H_2O', 'HO_2+H_2O \rightarrow H_2O_2+OH', 'CO+O(+M) \rightarrow CO_2(+M)', 'CO_2(+M) \rightarrow CO+O(+M)', 'CO+O_2 \rightarrow CO_2+O', 'CO_2+O \rightarrow CO+O_2', 'CO+HO_2 \rightarrow CO_2+OH', 'CO_2+OH \rightarrow CO+HO_2', 'CO+OH \rightarrow CO_2+H', 'CO_2+H \rightarrow CO+OH', 'HCO+M \rightarrow H+CO+M', 'H+CO+M \rightarrow HCO+M', 'HCO+O_2 \rightarrow CO+HO_2', 'CO+HO_2 \rightarrow HCO+O_2', 'HCO+H \rightarrow CO+H_2', 'CO+H_2 \rightarrow HCO+H', 'HCO+O \rightarrow CO+OH', 'CO+OH \rightarrow HCO+O', 'HCO+OH \rightarrow CO+H_2O', 'CO+H_2O \rightarrow HCO+OH', 'HCO+O \rightarrow CO_2+H', 'CO_2+H \rightarrow HCO+O', 'HCO+HO_2 \rightarrow CO_2+OH+H', 'CO_2+OH+H \rightarrow HCO+HO_2', 'HCO+CH_3 \rightarrow CO+CH_4', 'CO+CH_4 \rightarrow HCO+CH_3', 'HCO+HCO \rightarrow H_2+CO+CO', 'H_2+CO+CO \rightarrow HCO+HCO', 'HCO+HCO \rightarrow CH_2O+CO', 'CH_2O+CO \rightarrow HCO+HCO', 'HCO+O_2 \rightarrow formylperoxy', 'formylperoxy \rightarrow HCO+O_2', 'CH_2O+formylperoxy \rightarrow HCO+formylooh', 'HCO+formylooh \rightarrow CH_2O+formylperoxy', 'formylooh \rightarrow formyloxy+OH', 'formyloxy+OH \rightarrow formylooh', 'H+CO_2+M \rightarrow formyloxy+M', 'formyloxy+M \rightarrow H+CO_2+M', 'CH_2O+M \rightarrow HCO+H+M', 'HCO+H+M \rightarrow CH_2O+M', 'CH_2O+M \rightarrow CO+H_2+M', 'CO+H_2+M \rightarrow CH_2O+M', 'CH_2O+H \rightarrow HCO+H_2', 'HCO+H_2 \rightarrow CH_2O+H', 'CH_2O+O \rightarrow HCO+OH', 'HCO+OH \rightarrow CH_2O+O', 'CH_2O+OH \rightarrow HCO+H_2O', 'HCO+H_2O \rightarrow CH_2O+OH', 'CH_2O+O_2 \rightarrow HCO+HO_2', 'HCO+HO_2 \rightarrow CH_2O+O_2', 'CH_2O+HO_2 \rightarrow HCO+H_2O_2', 'HCO+H_2O_2 \rightarrow CH_2O+HO_2', 'CH_2O+CH_3 \rightarrow HCO+CH_4', 'HCO+CH_4 \rightarrow CH_2O+CH_3', 'CH_2O+HO_2 \rightarrow OCH_2OOH', 'OCH_2OOH \rightarrow CH_2O+HO_2', 'OCH_2OOH \rightarrow HOCH_2OO', 'HOCH_2OO \rightarrow OCH_2OOH', 'HOCH_2OO+HO_2 \rightarrow HOCH_2OOH+O_2', 'HOCH_2OOH+O_2 \rightarrow HOCH_2OO+HO_2', 'HOCH_2O+OH \rightarrow HOCH_2OOH', 'HOCH_2OOH \rightarrow HOCH_2O+OH', 'CH_3+O \rightarrow CH_2O+H', 'CH_2O+H \rightarrow CH_3+O', 'CH_3+O_2 \rightarrow CH_3O+O', 'CH_3O+O \rightarrow CH_3+O_2', 'CH_3+HO_2 \rightarrow CH_3O+OH', 'CH_3O+OH \rightarrow CH_3+HO_2', 'CH_3+HO_2 \rightarrow CH_4+O_2', 'CH_4+O_2 \rightarrow CH_3+HO_2', 'CH_3+CH_3(+M) \rightarrow C_2H_6(+M)', 'C_2H_6(+M) \rightarrow CH_3+CH_3(+M)', 'CH_3+H(+M) \rightarrow CH_4(+M)', 'CH_4(+M) \rightarrow CH_3+H(+M)', 'CH_4+H \rightarrow CH_3+H_2', 'CH_3+H_2 \rightarrow CH_4+H', 'CH_4+O \rightarrow CH_3+OH', 'CH_3+OH \rightarrow CH_4+O', 'CH_4+OH \rightarrow CH_3+H_2O', 'CH_3+H_2O \rightarrow CH_4+OH', 'CH_4+HO_2 \rightarrow CH_3+H_2O_2', 'CH_3+H_2O_2 \rightarrow CH_4+HO_2', 'CH_3+CH_3OH \rightarrow CH_4+CH_3O', 'CH_4+CH_3O \rightarrow CH_3+CH_3OH', 'CH_3O+CH_3 \rightarrow CH_2O+CH_4', 'CH_2O+CH_4 \rightarrow CH_3O+CH_3', 'CH_3O+H \rightarrow CH_2O+H_2', 'CH_2O+H_2 \rightarrow CH_3O+H', 'CH_3+O_2(+M) \rightarrow CH_3OO(+M)', 'CH_3OO(+M) \rightarrow CH_3+O_2(+M)', 'CH_3OO+CH_2O \rightarrow CH_3OOH+HCO', 'CH_3OOH+HCO \rightarrow CH_3OO+CH_2O', 'CH_4+CH_3OO \rightarrow CH_3+CH_3OOH', 'CH_3+CH_3OOH \rightarrow CH_4+CH_3OO', 'CH_3OH+CH_3OO \rightarrow CH_2OH+CH_3OOH', 'CH_2OH+CH_3OOH \rightarrow CH_3OH+CH_3OO', 'CH_3OO+CH_3 \rightarrow CH_3O+CH_3O', 'CH_3O+CH_3O \rightarrow CH_3OO+CH_3', 'CH_3OO+HO_2 \rightarrow CH_3OOH+O_2', 'CH_3OOH+O_2 \rightarrow CH_3OO+HO_2', 'CH_3OO+CH_3OO \rightarrow CH_2O+CH_3OH+O_2', 'CH_2O+CH_3OH+O_2 \rightarrow CH_3OO+CH_3OO', 'CH_3OO+CH_3OO \rightarrow O_2+CH_3O+CH_3O', 'O_2+CH_3O+CH_3O \rightarrow CH_3OO+CH_3OO', 'CH_3OO+H \rightarrow CH_3O+OH', 'CH_3O+OH \rightarrow CH_3OO+H', 'CH_3OO+O \rightarrow CH_3O+O_2', 'CH_3O+O_2 \rightarrow CH_3OO+O', 'CH_3OO+OH \rightarrow CH_3OH+O_2', 'CH_3OH+O_2 \rightarrow CH_3OO+OH', 'CH_3OOH \rightarrow CH_3O+OH', 'CH_3O+OH \rightarrow CH_3OOH', 'CH_2OH+M \rightarrow CH_2O+H+M', 'CH_2O+H+M \rightarrow CH_2OH+M', 'CH_2OH+H \rightarrow CH_2O+H_2', 'CH_2O+H_2 \rightarrow CH_2OH+H', 'CH_2OH+H \rightarrow CH_3+OH', 'CH_3+OH \rightarrow CH_2OH+H', 'CH_2OH+O \rightarrow CH_2O+OH', 'CH_2O+OH \rightarrow CH_2OH+O', 'CH_2OH+OH \rightarrow CH_2O+H_2O', 'CH_2O+H_2O \rightarrow CH_2OH+OH', 'CH_2OH+O_2 \rightarrow CH_2O+HO_2', 'CH_2O+HO_2 \rightarrow CH_2OH+O_2', 'CH_2OH+HO_2 \rightarrow CH_2O+H_2O_2', 'CH_2O+H_2O_2 \rightarrow CH_2OH+HO_2', 'CH_2OH+HCO \rightarrow CH_3OH+CO', 'CH_3OH+CO \rightarrow CH_2OH+HCO', 'CH_2OH+HCO \rightarrow CH_2O+CH_2O', 'CH_2O+CH_2O \rightarrow CH_2OH+HCO', '2CH_2OH \rightarrow CH_3OH+CH_2O', 'CH_3OH+CH_2O \rightarrow 2CH_2OH', 'CH_2OH+CH_3O \rightarrow CH_3OH+CH_2O', 'CH_3OH+CH_2O \rightarrow CH_2OH+CH_3O', 'CH_2OH+HO_2 \rightarrow HOCH_2O+OH', 'HOCH_2O+OH \rightarrow CH_2OH+HO_2', 'CH_3O+M \rightarrow CH_2O+H+M', 'CH_2O+H+M \rightarrow CH_3O+M', 'CH_3O+H \rightarrow CH_3+OH', 'CH_3+OH \rightarrow CH_3O+H', 'CH_3O+O \rightarrow CH_2O+OH', 'CH_2O+OH \rightarrow CH_3O+O', 'CH_3O+OH \rightarrow CH_2O+H_2O', 'CH_2O+H_2O \rightarrow CH_3O+OH', 'CH_3O+O_2 \rightarrow CH_2O+HO_2', 'CH_2O+HO_2 \rightarrow CH_3O+O_2', 'CH_3O+HO_2 \rightarrow CH_2O+H_2O_2', 'CH_2O+H_2O_2 \rightarrow CH_3O+HO_2', 'CH_3O+CO \rightarrow CH_3+CO_2', 'CH_3+CO_2 \rightarrow CH_3O+CO', 'CH_3O+HCO \rightarrow CH_3OH+CO', 'CH_3OH+CO \rightarrow CH_3O+HCO', '2CH_3O \rightarrow CH_3OH+CH_2O', 'CH_3OH+CH_2O \rightarrow 2CH_3O', 'OH+CH_3(+M) \rightarrow CH_3OH(+M)', 'CH_3OH(+M) \rightarrow OH+CH_3(+M)', 'H+CH_2OH(+M) \rightarrow CH_3OH(+M)', 'CH_3OH(+M) \rightarrow H+CH_2OH(+M)', 'H+CH_3O(+M) \rightarrow CH_3OH(+M)', 'CH_3OH(+M) \rightarrow H+CH_3O(+M)', 'CH_3OH+H \rightarrow CH_2OH+H_2', 'CH_2OH+H_2 \rightarrow CH_3OH+H', 'CH_3OH+H \rightarrow CH_3O+H_2', 'CH_3O+H_2 \rightarrow CH_3OH+H', 'CH_3OH+O \rightarrow CH_2OH+OH', 'CH_2OH+OH \rightarrow CH_3OH+O', 'CH_3OH+OH \rightarrow CH_3O+H_2O', 'CH_3O+H_2O \rightarrow CH_3OH+OH', 'CH_3OH+OH \rightarrow CH_2OH+H_2O', 'CH_2OH+H_2O \rightarrow CH_3OH+OH', 'CH_3OH+O_2 \rightarrow CH_2OH+HO_2', 'CH_2OH+HO_2 \rightarrow CH_3OH+O_2', 'CH_3OH+HCO \rightarrow CH_2OH+CH_2O', 'CH_2OH+CH_2O \rightarrow CH_3OH+HCO', 'CH_3OH+HO_2 \rightarrow CH_2OH+H_2O_2', 'CH_2OH+H_2O_2 \rightarrow CH_3OH+HO_2', 'CH_3OH+CH_3 \rightarrow CH_2OH+CH_4', 'CH_2OH+CH_4 \rightarrow CH_3OH+CH_3', 'CH_3O+CH_3OH \rightarrow CH_3OH+CH_2OH', 'CH_3OH+CH_2OH \rightarrow CH_3O+CH_3OH', 'CH_3+CH_3 \rightarrow H+C_2H_5', 'H+C_2H_5 \rightarrow CH_3+CH_3', 'CH_4+CH_2 \rightarrow CH_3+CH_3', 'CH_3+CH_3 \rightarrow CH_4+CH_2', 'CH_3+OH \rightarrow CH_2+H_2O', 'CH_2+H_2O \rightarrow CH_3+OH', 'CH_3+CH_2 \rightarrow C_2H_4+H', 'C_2H_4+H \rightarrow CH_3+CH_2', 'CH_2+H(+M) \rightarrow CH_3(+M)', 'CH_3(+M) \rightarrow CH_2+H(+M)', 'CH_2+O \rightarrow HCO+H', 'HCO+H \rightarrow CH_2+O', 'CH_2+OH \rightarrow CH_2O+H', 'CH_2O+H \rightarrow CH_2+OH', 'CH_2+H_2 \rightarrow H+CH_3', 'H+CH_3 \rightarrow CH_2+H_2', 'CH_2+O_2 \rightarrow HCO+OH', 'HCO+OH \rightarrow CH_2+O_2', 'CH_2+HO_2 \rightarrow CH_2O+OH', 'CH_2O+OH \rightarrow CH_2+HO_2', 'CH_2+CO(+M) \rightarrow ketene(+M)', 'ketene(+M) \rightarrow CH_2+CO(+M)', 'CH_2+CH_2 \rightarrow C_2H_2+H_2', 'C_2H_2+H_2 \rightarrow CH_2+CH_2', 'CH_2(S)+M \rightarrow CH_2+M', 'CH_2+M \rightarrow CH_2(S)+M', 'CH_2(S)+H_2O \rightarrow CH_2+H_2O', 'CH_2+H_2O \rightarrow CH_2(S)+H_2O', 'CH_2(S)+CO \rightarrow CH_2+CO', 'CH_2+CO \rightarrow CH_2(S)+CO', 'CH_2(S)+CO_2 \rightarrow CH_2+CO_2', 'CH_2+CO_2 \rightarrow CH_2(S)+CO_2', 'CH_2(S)+AR \rightarrow CH_2+AR', 'CH_2+AR \rightarrow CH_2(S)+AR', 'CH_2(S)+O \rightarrow CO+H_2', 'CO+H_2 \rightarrow CH_2(S)+O', 'CH_2(S)+O \rightarrow HCO+H', 'HCO+H \rightarrow CH_2(S)+O', 'CH_2(S)+OH \rightarrow CH_2O+H', 'CH_2O+H \rightarrow CH_2(S)+OH', 'CH_2(S)+H_2 \rightarrow CH_3+H', 'CH_3+H \rightarrow CH_2(S)+H_2', 'CH_2(S)+O_2 \rightarrow H+OH+CO', 'H+OH+CO \rightarrow CH_2(S)+O_2', 'CH_2(S)+O_2 \rightarrow CO+H_2O', 'CO+H_2O \rightarrow CH_2(S)+O_2', 'CH_2(S)+CO_2 \rightarrow CH_2O+CO', 'CH_2O+CO \rightarrow CH_2(S)+CO_2', 'CH_2(S)+C_2H_6 \rightarrow CH_3+C_2H_5', 'CH_3+C_2H_5 \rightarrow CH_2(S)+C_2H_6', 'CH_2(S)+ketene \rightarrow C_2H_4+CO', 'C_2H_4+CO \rightarrow CH_2(S)+ketene', 'H+HCCO \rightarrow CH_2(S)+CO', 'CH_2(S)+CO \rightarrow H+HCCO', 'CH_2(S)+H \rightarrow CH+H_2', 'CH+H_2 \rightarrow CH_2(S)+H', 'CH_2+H \rightarrow CH+H_2', 'CH+H_2 \rightarrow CH_2+H', 'CH_2+OH \rightarrow CH+H_2O', 'CH+H_2O \rightarrow CH_2+OH', 'CH+O_2 \rightarrow HCO+O', 'HCO+O \rightarrow CH+O_2', 'CH+H \rightarrow C+H_2', 'C+H_2 \rightarrow CH+H', 'CH+O \rightarrow CO+H', 'CO+H \rightarrow CH+O', 'CH+OH \rightarrow HCO+H', 'HCO+H \rightarrow CH+OH', 'CH+H_2O \rightarrow H+CH_2O', 'H+CH_2O \rightarrow CH+H_2O', 'CH+CO_2 \rightarrow HCO+CO', 'HCO+CO \rightarrow CH+CO_2', 'HCCO+M \rightarrow CH+CO+M', 'CH+CO+M \rightarrow HCCO+M', 'CH+CH_2O \rightarrow H+ketene', 'H+ketene \rightarrow CH+CH_2O', 'CH+HCCO \rightarrow CO+C_2H_2', 'CO+C_2H_2 \rightarrow CH+HCCO', 'CH+CH_4 \rightarrow C_2H_4+H', 'C_2H_4+H \rightarrow CH+CH_4', 'C_2H_6+CH \rightarrow C_2H_5+CH_2', 'C_2H_5+CH_2 \rightarrow C_2H_6+CH', 'C_2H_5+H(+M) \rightarrow C_2H_6(+M)', 'C_2H_6(+M) \rightarrow C_2H_5+H(+M)', 'C_2H_6+H \rightarrow C_2H_5+H_2', 'C_2H_5+H_2 \rightarrow C_2H_6+H', 'C_2H_6+O \rightarrow C_2H_5+OH', 'C_2H_5+OH \rightarrow C_2H_6+O', 'C_2H_6+OH \rightarrow C_2H_5+H_2O', 'C_2H_5+H_2O \rightarrow C_2H_6+OH', 'C_2H_6+O_2 \rightarrow C_2H_5+HO_2', 'C_2H_5+HO_2 \rightarrow C_2H_6+O_2', 'C_2H_6+CH_3 \rightarrow C_2H_5+CH_4', 'C_2H_5+CH_4 \rightarrow C_2H_6+CH_3', 'C_2H_6+HO_2 \rightarrow C_2H_5+H_2O_2', 'C_2H_5+H_2O_2 \rightarrow C_2H_6+HO_2', 'C_2H_6+CH_3OO \rightarrow C_2H_5+CH_3OOH', 'C_2H_5+CH_3OOH \rightarrow C_2H_6+CH_3OO', 'C_2H_6+CH_3O \rightarrow C_2H_5+CH_3OH', 'C_2H_5+CH_3OH \rightarrow C_2H_6+CH_3O', 'H+C_2H_4(+M) \rightarrow C_2H_5(+M)', 'C_2H_5(+M) \rightarrow H+C_2H_4(+M)', 'H_2+CH_3OO \rightarrow H+CH_3OOH', 'H+CH_3OOH \rightarrow H_2+CH_3OO', 'H_2+CH_3CH_2OO \rightarrow H+CH_3CH_2OOH', 'H+CH_3CH_2OOH \rightarrow H_2+CH_3CH_2OO', 'C_2H_4+C_2H_4 \rightarrow C_2H_5+C_2H_3', 'C_2H_5+C_2H_3 \rightarrow C_2H_4+C_2H_4', 'CH_3+C_2H_5 \rightarrow CH_4+C_2H_4', 'CH_4+C_2H_4 \rightarrow CH_3+C_2H_5', 'C_2H_5+H \rightarrow C_2H_4+H_2', 'C_2H_4+H_2 \rightarrow C_2H_5+H', 'C_2H_5+O \rightarrow acetaldehyde+H', 'acetaldehyde+H \rightarrow C_2H_5+O', 'C_2H_5+HO_2 \rightarrow ethoxy+OH', 'ethoxy+OH \rightarrow C_2H_5+HO_2', 'CH_3OO+C_2H_5 \rightarrow CH_3O+ethoxy', 'CH_3O+ethoxy \rightarrow CH_3OO+C_2H_5', 'ethoxy+O_2 \rightarrow acetaldehyde+HO_2', 'acetaldehyde+HO_2 \rightarrow ethoxy+O_2', 'CH_3+CH_2O \rightarrow ethoxy', 'ethoxy \rightarrow CH_3+CH_2O', 'acetaldehyde+H \rightarrow ethoxy', 'ethoxy \rightarrow acetaldehyde+H', 'C_2H_5+O_2 \rightarrow CH_3CH_2OO', 'CH_3CH_2OO \rightarrow C_2H_5+O_2', 'CH_3CH_2OO+CH_2O \rightarrow CH_3CH_2OOH+HCO', 'CH_3CH_2OOH+HCO \rightarrow CH_3CH_2OO+CH_2O', 'CH_4+CH_3CH_2OO \rightarrow CH_3+CH_3CH_2OOH', 'CH_3+CH_3CH_2OOH \rightarrow CH_4+CH_3CH_2OO', 'CH_3OH+CH_3CH_2OO \rightarrow CH_2OH+CH_3CH_2OOH', 'CH_2OH+CH_3CH_2OOH \rightarrow CH_3OH+CH_3CH_2OO', 'CH_3CH_2OO+HO_2 \rightarrow CH_3CH_2OOH+O_2', 'CH_3CH_2OOH+O_2 \rightarrow CH_3CH_2OO+HO_2', 'C_2H_6+CH_3CH_2OO \rightarrow C_2H_5+CH_3CH_2OOH', 'C_2H_5+CH_3CH_2OOH \rightarrow C_2H_6+CH_3CH_2OO', 'CH_3CH_2OOH \rightarrow ethoxy+OH', 'ethoxy+OH \rightarrow CH_3CH_2OOH', 'C_2H_5+O_2 \rightarrow CH_2CH_2OOH', 'CH_2CH_2OOH \rightarrow C_2H_5+O_2', 'C_2H_5+O_2 \rightarrow C_2H_4+HO_2', 'C_2H_4+HO_2 \rightarrow C_2H_5+O_2', 'C_2H_5+O_2 \rightarrow oxirane+OH', 'oxirane+OH \rightarrow C_2H_5+O_2', 'C_2H_5+O_2 \rightarrow acetaldehyde+OH', 'acetaldehyde+OH \rightarrow C_2H_5+O_2', 'CH_2CH_2OOH \rightarrow CH_3CH_2OO', 'CH_3CH_2OO \rightarrow CH_2CH_2OOH', 'CH_3CH_2OO \rightarrow acetaldehyde+OH', 'acetaldehyde+OH \rightarrow CH_3CH_2OO', 'CH_3CH_2OO \rightarrow C_2H_4+HO_2', 'C_2H_4+HO_2 \rightarrow CH_3CH_2OO', 'CH_3CH_2OO \rightarrow oxirane+OH', 'oxirane+OH \rightarrow CH_3CH_2OO', 'CH_2CH_2OOH \rightarrow oxirane+OH', 'oxirane+OH \rightarrow CH_2CH_2OOH', 'CH_2CH_2OOH \rightarrow C_2H_4+HO_2', 'C_2H_4+HO_2 \rightarrow CH_2CH_2OOH', 'CH_2CH_2OOH \rightarrow acetaldehyde+OH', 'acetaldehyde+OH \rightarrow CH_2CH_2OOH', 'oxirane \rightarrow CH_3+HCO', 'CH_3+HCO \rightarrow oxirane', 'oxirane \rightarrow acetaldehyde', 'acetaldehyde \rightarrow oxirane', 'oxirane+OH \rightarrow oxiranyl+H_2O', 'oxiranyl+H_2O \rightarrow oxirane+OH', 'oxirane+H \rightarrow oxiranyl+H_2', 'oxiranyl+H_2 \rightarrow oxirane+H', 'oxirane+HO_2 \rightarrow oxiranyl+H_2O_2', 'oxiranyl+H_2O_2 \rightarrow oxirane+HO_2', 'oxirane+CH_3OO \rightarrow oxiranyl+CH_3OOH', 'oxiranyl+CH_3OOH \rightarrow oxirane+CH_3OO', 'oxirane+CH_3CH_2OO \rightarrow oxiranyl+CH_3CH_2OOH', 'oxiranyl+CH_3CH_2OOH \rightarrow oxirane+CH_3CH_2OO', 'oxirane+CH_3 \rightarrow oxiranyl+CH_4', 'oxiranyl+CH_4 \rightarrow oxirane+CH_3', 'oxirane+CH_3O \rightarrow oxiranyl+CH_3OH', 'oxiranyl+CH_3OH \rightarrow oxirane+CH_3O', 'oxiranyl \rightarrow acetyl', 'acetyl \rightarrow oxiranyl', 'oxiranyl \rightarrow vinoxy', 'vinoxy \rightarrow oxiranyl', 'CH_3+HCO \rightarrow acetaldehyde', 'acetaldehyde \rightarrow CH_3+HCO', 'acetaldehyde+H \rightarrow acetyl+H_2', 'acetyl+H_2 \rightarrow acetaldehyde+H', 'acetaldehyde+O \rightarrow acetyl+OH', 'acetyl+OH \rightarrow acetaldehyde+O', 'acetaldehyde+OH \rightarrow acetyl+H_2O', 'acetyl+H_2O \rightarrow acetaldehyde+OH', 'acetaldehyde+O_2 \rightarrow acetyl+HO_2', 'acetyl+HO_2 \rightarrow acetaldehyde+O_2', 'acetaldehyde+CH_3 \rightarrow acetyl+CH_4', 'acetyl+CH_4 \rightarrow acetaldehyde+CH_3', 'acetaldehyde+HO_2 \rightarrow acetyl+H_2O_2', 'acetyl+H_2O_2 \rightarrow acetaldehyde+HO_2', 'CH_3OO+acetaldehyde \rightarrow CH_3OOH+acetyl', 'CH_3OOH+acetyl \rightarrow CH_3OO+acetaldehyde', 'acetaldehyde+acetylperoxy \rightarrow acetyl+CH_3CO_3H', 'acetyl+CH_3CO_3H \rightarrow acetaldehyde+acetylperoxy', 'acetaldehyde+OH \rightarrow vinoxy+H_2O', 'vinoxy+H_2O \rightarrow acetaldehyde+OH', 'acetyl(+M) \rightarrow CH_3+CO(+M)', 'CH_3+CO(+M) \rightarrow acetyl(+M)', 'acetyl+H \rightarrow ketene+H_2', 'ketene+H_2 \rightarrow acetyl+H', 'acetyl+O \rightarrow ketene+OH', 'ketene+OH \rightarrow acetyl+O', 'acetyl+CH_3 \rightarrow ketene+CH_4', 'ketene+CH_4 \rightarrow acetyl+CH_3', 'acetyl+O_2 \rightarrow acetylperoxy', 'acetylperoxy \rightarrow acetyl+O_2', 'acetylperoxy+HO_2 \rightarrow CH_3CO_3H+O_2', 'CH_3CO_3H+O_2 \rightarrow acetylperoxy+HO_2', 'H_2O_2+acetylperoxy \rightarrow HO_2+CH_3CO_3H', 'HO_2+CH_3CO_3H \rightarrow H_2O_2+acetylperoxy', 'CH_4+acetylperoxy \rightarrow CH_3+CH_3CO_3H', 'CH_3+CH_3CO_3H \rightarrow CH_4+acetylperoxy', 'CH_2O+acetylperoxy \rightarrow HCO+CH_3CO_3H', 'HCO+CH_3CO_3H \rightarrow CH_2O+acetylperoxy', 'C_2H_6+acetylperoxy \rightarrow C_2H_5+CH_3CO_3H', 'C_2H_5+CH_3CO_3H \rightarrow C_2H_6+acetylperoxy', 'CH_3CO_3H \rightarrow acetyloxy+OH', 'acetyloxy+OH \rightarrow CH_3CO_3H', 'acetyloxy+M \rightarrow CH_3+CO_2+M', 'CH_3+CO_2+M \rightarrow acetyloxy+M', 'ketene+H \rightarrow vinoxy', 'vinoxy \rightarrow ketene+H', 'vinoxy+O_2 \rightarrow CH_2O+CO+OH', 'CH_2O+CO+OH \rightarrow vinoxy+O_2', 'ketene+H \rightarrow CH_3+CO', 'CH_3+CO \rightarrow ketene+H', 'ketene+H \rightarrow HCCO+H_2', 'HCCO+H_2 \rightarrow ketene+H', 'ketene+O \rightarrow CH_2+CO_2', 'CH_2+CO_2 \rightarrow ketene+O', 'ketene+O \rightarrow HCCO+OH', 'HCCO+OH \rightarrow ketene+O', 'ketene+OH \rightarrow HCCO+H_2O', 'HCCO+H_2O \rightarrow ketene+OH', 'ketene+OH \rightarrow CH_2OH+CO', 'CH_2OH+CO \rightarrow ketene+OH', 'HCCO+OH \rightarrow H_2+CO+CO', 'H_2+CO+CO \rightarrow HCCO+OH', 'HCCO+O \rightarrow H+CO+CO', 'H+CO+CO \rightarrow HCCO+O', 'HCCO+O_2 \rightarrow OH+CO+CO', 'OH+CO+CO \rightarrow HCCO+O_2', 'C_2H_3+H(+M) \rightarrow C_2H_4(+M)', 'C_2H_4(+M) \rightarrow C_2H_3+H(+M)', 'C_2H_4(+M) \rightarrow C_2H_2+H_2(+M)', 'C_2H_2+H_2(+M) \rightarrow C_2H_4(+M)', 'C_2H_4+H \rightarrow C_2H_3+H_2', 'C_2H_3+H_2 \rightarrow C_2H_4+H', 'C_2H_4+O \rightarrow CH_3+HCO', 'CH_3+HCO \rightarrow C_2H_4+O', 'C_2H_4+O \rightarrow vinoxy+H', 'vinoxy+H \rightarrow C_2H_4+O', 'C_2H_4+OH \rightarrow C_2H_3+H_2O', 'C_2H_3+H_2O \rightarrow C_2H_4+OH', 'C_2H_4+CH_3 \rightarrow C_2H_3+CH_4', 'C_2H_3+CH_4 \rightarrow C_2H_4+CH_3', 'C_2H_4+O_2 \rightarrow C_2H_3+HO_2', 'C_2H_3+HO_2 \rightarrow C_2H_4+O_2', 'C_2H_4+CH_3O \rightarrow C_2H_3+CH_3OH', 'C_2H_3+CH_3OH \rightarrow C_2H_4+CH_3O', 'C_2H_4+CH_3OO \rightarrow C_2H_3+CH_3OOH', 'C_2H_3+CH_3OOH \rightarrow C_2H_4+CH_3OO', 'C_2H_4+CH_3CH_2OO \rightarrow C_2H_3+CH_3CH_2OOH', 'C_2H_3+CH_3CH_2OOH \rightarrow C_2H_4+CH_3CH_2OO', 'C_2H_4+acetylperoxy \rightarrow C_2H_3+CH_3CO_3H', 'C_2H_3+CH_3CO_3H \rightarrow C_2H_4+acetylperoxy', 'C_2H_4+CH_3OO \rightarrow oxirane+CH_3O', 'oxirane+CH_3O \rightarrow C_2H_4+CH_3OO', 'C_2H_4+CH_3CH_2OO \rightarrow oxirane+ethoxy', 'oxirane+ethoxy \rightarrow C_2H_4+CH_3CH_2OO', 'C_2H_4+HO_2 \rightarrow oxirane+OH', 'oxirane+OH \rightarrow C_2H_4+HO_2', 'C_2H_2+H(+M) \rightarrow C_2H_3(+M)', 'C_2H_3(+M) \rightarrow C_2H_2+H(+M)', 'C_2H_3+O_2 \rightarrow HCO+CH_2O', 'HCO+CH_2O \rightarrow C_2H_3+O_2', 'C_2H_3+O_2 \rightarrow HO_2+C_2H_2', 'HO_2+C_2H_2 \rightarrow C_2H_3+O_2', 'C_2H_3+O_2 \rightarrow O+vinoxy', 'O+vinoxy \rightarrow C_2H_3+O_2', 'CH_3+C_2H_3 \rightarrow CH_4+C_2H_2', 'CH_4+C_2H_2 \rightarrow CH_3+C_2H_3', 'C_2H_3+H \rightarrow C_2H_2+H_2', 'C_2H_2+H_2 \rightarrow C_2H_3+H', 'C_2H_3+OH \rightarrow C_2H_2+H_2O', 'C_2H_2+H_2O \rightarrow C_2H_3+OH', 'C_2H_2+O_2 \rightarrow HCCO+OH', 'HCCO+OH \rightarrow C_2H_2+O_2', 'C_2H_2+O \rightarrow CH_2+CO', 'CH_2+CO \rightarrow C_2H_2+O', 'C_2H_2+O \rightarrow HCCO+H', 'HCCO+H \rightarrow C_2H_2+O', 'C_2H_2+OH \rightarrow ketene+H', 'ketene+H \rightarrow C_2H_2+OH', 'C_2H_2+OH \rightarrow CH_3+CO', 'CH_3+CO \rightarrow C_2H_2+OH', 'OH+C_2H_2 \rightarrow H+ethynol', 'H+ethynol \rightarrow OH+C_2H_2', 'H+ethynol \rightarrow H+ketene', 'H+ketene \rightarrow H+ethynol', 'ethanol(+M) \rightarrow CH_2OH+CH_3(+M)', 'CH_2OH+CH_3(+M) \rightarrow ethanol(+M)', 'ethanol(+M) \rightarrow C_2H_5+OH(+M)', 'C_2H_5+OH(+M) \rightarrow ethanol(+M)', 'ethanol(+M) \rightarrow C_2H_4+H_2O(+M)', 'C_2H_4+H_2O(+M) \rightarrow ethanol(+M)', 'ethanol(+M) \rightarrow acetaldehyde+H_2(+M)', 'acetaldehyde+H_2(+M) \rightarrow ethanol(+M)', 'ethanol+O_2 \rightarrow CH_2CH_2OH+HO_2', 'CH_2CH_2OH+HO_2 \rightarrow ethanol+O_2', 'ethanol+O_2 \rightarrow CH_3CHOH+HO_2', 'CH_3CHOH+HO_2 \rightarrow ethanol+O_2', 'ethanol+OH \rightarrow CH_2CH_2OH+H_2O', 'CH_2CH_2OH+H_2O \rightarrow ethanol+OH', 'ethanol+OH \rightarrow CH_3CHOH+H_2O', 'CH_3CHOH+H_2O \rightarrow ethanol+OH', 'ethanol+OH \rightarrow ethoxy+H_2O', 'ethoxy+H_2O \rightarrow ethanol+OH', 'ethanol+H \rightarrow CH_2CH_2OH+H_2', 'CH_2CH_2OH+H_2 \rightarrow ethanol+H', 'ethanol+H \rightarrow CH_3CHOH+H_2', 'CH_3CHOH+H_2 \rightarrow ethanol+H', 'ethanol+H \rightarrow ethoxy+H_2', 'ethoxy+H_2 \rightarrow ethanol+H', 'ethanol+HO_2 \rightarrow CH_2CH_2OH+H_2O_2', 'CH_2CH_2OH+H_2O_2 \rightarrow ethanol+HO_2', 'ethanol+HO_2 \rightarrow CH_3CHOH+H_2O_2', 'CH_3CHOH+H_2O_2 \rightarrow ethanol+HO_2', 'ethanol+HO_2 \rightarrow ethoxy+H_2O_2', 'ethoxy+H_2O_2 \rightarrow ethanol+HO_2', 'ethanol+CH_3OO \rightarrow CH_2CH_2OH+CH_3OOH', 'CH_2CH_2OH+CH_3OOH \rightarrow ethanol+CH_3OO', 'ethanol+CH_3OO \rightarrow CH_3CHOH+CH_3OOH', 'CH_3CHOH+CH_3OOH \rightarrow ethanol+CH_3OO', 'ethanol+CH_3OO \rightarrow ethoxy+CH_3OOH', 'ethoxy+CH_3OOH \rightarrow ethanol+CH_3OO', 'ethanol+O \rightarrow CH_2CH_2OH+OH', 'CH_2CH_2OH+OH \rightarrow ethanol+O', 'ethanol+O \rightarrow CH_3CHOH+OH', 'CH_3CHOH+OH \rightarrow ethanol+O', 'ethanol+O \rightarrow ethoxy+OH', 'ethoxy+OH \rightarrow ethanol+O', 'ethanol+CH_3 \rightarrow CH_2CH_2OH+CH_4', 'CH_2CH_2OH+CH_4 \rightarrow ethanol+CH_3', 'ethanol+CH_3 \rightarrow CH_3CHOH+CH_4', 'CH_3CHOH+CH_4 \rightarrow ethanol+CH_3', 'ethanol+CH_3 \rightarrow ethoxy+CH_4', 'ethoxy+CH_4 \rightarrow ethanol+CH_3', 'ethanol+C_2H_5 \rightarrow CH_2CH_2OH+C_2H_6', 'CH_2CH_2OH+C_2H_6 \rightarrow ethanol+C_2H_5', 'ethanol+C_2H_5 \rightarrow CH_3CHOH+C_2H_6', 'CH_3CHOH+C_2H_6 \rightarrow ethanol+C_2H_5', 'C_2H_4+OH \rightarrow CH_2CH_2OH', 'CH_2CH_2OH \rightarrow C_2H_4+OH', 'CH_3CHOH+M \rightarrow acetaldehyde+H+M', 'acetaldehyde+H+M \rightarrow CH_3CHOH+M', 'O_2C_2H_4OH \rightarrow CH_2CH_2OH+O_2', 'CH_2CH_2OH+O_2 \rightarrow O_2C_2H_4OH', 'O_2C_2H_4OH \rightarrow OH+CH_2O+CH_2O', 'OH+CH_2O+CH_2O \rightarrow O_2C_2H_4OH', 'CH_3CHOH+O_2 \rightarrow acetaldehyde+HO_2', 'acetaldehyde+HO_2 \rightarrow CH_3CHOH+O_2', 'acetone(+M) \rightarrow acetyl+CH_3(+M)', 'acetyl+CH_3(+M) \rightarrow acetone(+M)', 'acetone+OH \rightarrow propen2oxy+H_2O', 'propen2oxy+H_2O \rightarrow acetone+OH', 'acetone+H \rightarrow propen2oxy+H_2', 'propen2oxy+H_2 \rightarrow acetone+H', 'acetone+O \rightarrow propen2oxy+OH', 'propen2oxy+OH \rightarrow acetone+O', 'acetone+CH_3 \rightarrow propen2oxy+CH_4', 'propen2oxy+CH_4 \rightarrow acetone+CH_3', 'acetone+CH_3O \rightarrow propen2oxy+CH_3OH', 'propen2oxy+CH_3OH \rightarrow acetone+CH_3O', 'acetone+O_2 \rightarrow propen2oxy+HO_2', 'propen2oxy+HO_2 \rightarrow acetone+O_2', 'acetone+HO_2 \rightarrow propen2oxy+H_2O_2', 'propen2oxy+H_2O_2 \rightarrow acetone+HO_2', 'acetone+CH_3OO \rightarrow propen2oxy+CH_3OOH', 'propen2oxy+CH_3OOH \rightarrow acetone+CH_3OO', 'propen2oxy \rightarrow ketene+CH_3', 'ketene+CH_3 \rightarrow propen2oxy', 'iR+O \rightarrow acetone+H', 'acetone+H \rightarrow iR+O', 'acetone+H \rightarrow iRO', 'iRO \rightarrow acetone+H', 'iRO+O_2 \rightarrow acetone+HO_2', 'acetone+HO_2 \rightarrow iRO+O_2', 'C_2H_3+HCO \rightarrow acrolein', 'acrolein \rightarrow C_2H_3+HCO', 'acrolein+H \rightarrow CH_2CHCO+H_2', 'CH_2CHCO+H_2 \rightarrow acrolein+H', 'acrolein+O \rightarrow CH_2CHCO+OH', 'CH_2CHCO+OH \rightarrow acrolein+O', 'acrolein+H \rightarrow C_2H_4+HCO', 'C_2H_4+HCO \rightarrow acrolein+H', 'acrolein+O \rightarrow ketene+HCO+H', 'ketene+HCO+H \rightarrow acrolein+O', 'acrolein+OH \rightarrow CH_2CHCO+H_2O', 'CH_2CHCO+H_2O \rightarrow acrolein+OH', 'acrolein+O_2 \rightarrow CH_2CHCO+HO_2', 'CH_2CHCO+HO_2 \rightarrow acrolein+O_2', 'acrolein+HO_2 \rightarrow CH_2CHCO+H_2O_2', 'CH_2CHCO+H_2O_2 \rightarrow acrolein+HO_2', 'acrolein+CH_3 \rightarrow CH_2CHCO+CH_4', 'CH_2CHCO+CH_4 \rightarrow acrolein+CH_3', 'acrolein+C_2H_3 \rightarrow CH_2CHCO+C_2H_4', 'CH_2CHCO+C_2H_4 \rightarrow acrolein+C_2H_3', 'acrolein+CH_3O \rightarrow CH_2CHCO+CH_3OH', 'CH_2CHCO+CH_3OH \rightarrow acrolein+CH_3O', 'acrolein+CH_3OO \rightarrow CH_2CHCO+CH_3OOH', 'CH_2CHCO+CH_3OOH \rightarrow acrolein+CH_3OO', 'C_2H_3+CO \rightarrow CH_2CHCO', 'CH_2CHCO \rightarrow C_2H_3+CO', 'CH_2CHCO+O_2 \rightarrow vinoxy+CO_2', 'vinoxy+CO_2 \rightarrow CH_2CHCO+O_2', 'CH_2CHCO+O \rightarrow C_2H_3+CO_2', 'C_2H_3+CO_2 \rightarrow CH_2CHCO+O', 'C_2H_5+HCO \rightarrow propanal', 'propanal \rightarrow C_2H_5+HCO', 'propanal+H \rightarrow propionyl+H_2', 'propionyl+H_2 \rightarrow propanal+H', 'propanal+O \rightarrow propionyl+OH', 'propionyl+OH \rightarrow propanal+O', 'propanal+OH \rightarrow propionyl+H_2O', 'propionyl+H_2O \rightarrow propanal+OH', 'propanal+CH_3 \rightarrow propionyl+CH_4', 'propionyl+CH_4 \rightarrow propanal+CH_3', 'propanal+HO_2 \rightarrow propionyl+H_2O_2', 'propionyl+H_2O_2 \rightarrow propanal+HO_2', 'propanal+CH_3O \rightarrow propionyl+CH_3OH', 'propionyl+CH_3OH \rightarrow propanal+CH_3O', 'propanal+CH_3OO \rightarrow propionyl+CH_3OOH', 'propionyl+CH_3OOH \rightarrow propanal+CH_3OO', 'propanal+C_2H_5 \rightarrow propionyl+C_2H_6', 'propionyl+C_2H_6 \rightarrow propanal+C_2H_5', 'propanal+ethoxy \rightarrow propionyl+ethanol', 'propionyl+ethanol \rightarrow propanal+ethoxy', 'propanal+CH_3CH_2OO \rightarrow propionyl+CH_3CH_2OOH', 'propionyl+CH_3CH_2OOH \rightarrow propanal+CH_3CH_2OO', 'propanal+O_2 \rightarrow propionyl+HO_2', 'propionyl+HO_2 \rightarrow propanal+O_2', 'propanal+acetylperoxy \rightarrow propionyl+CH_3CO_3H', 'propionyl+CH_3CO_3H \rightarrow propanal+acetylperoxy', 'propanal+C_2H_3 \rightarrow propionyl+C_2H_4', 'propionyl+C_2H_4 \rightarrow propanal+C_2H_3', 'C_2H_5+CO \rightarrow propionyl', 'propionyl \rightarrow C_2H_5+CO', 'CH_3OCH_3(+M) \rightarrow CH_3+CH_3O(+M)', 'CH_3+CH_3O(+M) \rightarrow CH_3OCH_3(+M)', 'CH_3OCH_3+OH \rightarrow CH_3OCH_2+H_2O', 'CH_3OCH_2+H_2O \rightarrow CH_3OCH_3+OH', 'CH_3OCH_3+H \rightarrow CH_3OCH_2+H_2', 'CH_3OCH_2+H_2 \rightarrow CH_3OCH_3+H', 'CH_3OCH_3+O \rightarrow CH_3OCH_2+OH', 'CH_3OCH_2+OH \rightarrow CH_3OCH_3+O', 'CH_3OCH_3+HO_2 \rightarrow CH_3OCH_2+H_2O_2', 'CH_3OCH_2+H_2O_2 \rightarrow CH_3OCH_3+HO_2', 'CH_3OCH_3+CH_3OO \rightarrow CH_3OCH_2+CH_3OOH', 'CH_3OCH_2+CH_3OOH \rightarrow CH_3OCH_3+CH_3OO', 'CH_3OCH_3+CH_3 \rightarrow CH_3OCH_2+CH_4', 'CH_3OCH_2+CH_4 \rightarrow CH_3OCH_3+CH_3', 'CH_3OCH_3+O_2 \rightarrow CH_3OCH_2+HO_2', 'CH_3OCH_2+HO_2 \rightarrow CH_3OCH_3+O_2', 'CH_3OCH_3+CH_3O \rightarrow CH_3OCH_2+CH_3OH', 'CH_3OCH_2+CH_3OH \rightarrow CH_3OCH_3+CH_3O', 'CH_3OCH_3+formylperoxy \rightarrow CH_3OCH_2+formylooh', 'CH_3OCH_2+formylooh \rightarrow CH_3OCH_3+formylperoxy', 'CH_3OCH_2 \rightarrow CH_2O+CH_3', 'CH_2O+CH_3 \rightarrow CH_3OCH_2', 'CH_3OCH_2+CH_3O \rightarrow CH_3OCH_3+CH_2O', 'CH_3OCH_3+CH_2O \rightarrow CH_3OCH_2+CH_3O', 'CH_3OCH_2+CH_2O \rightarrow CH_3OCH_3+HCO', 'CH_3OCH_3+HCO \rightarrow CH_3OCH_2+CH_2O', 'CH_3OCH_2+acetaldehyde \rightarrow CH_3OCH_3+acetyl', 'CH_3OCH_3+acetyl \rightarrow CH_3OCH_2+acetaldehyde', 'nROO+RH \rightarrow nROOH+nR', 'nROOH+nR \rightarrow nROO+RH', 'iROO+RH \rightarrow iROOH+nR', 'iROOH+nR \rightarrow iROO+RH', 'nROO+RH \rightarrow nROOH+iR', 'nROOH+iR \rightarrow nROO+RH', 'iROO+RH \rightarrow iROOH+iR', 'iROOH+iR \rightarrow iROO+RH', 'nROOH \rightarrow nRO+OH', 'nRO+OH \rightarrow nROOH', 'iROOH \rightarrow iRO+OH', 'iRO+OH \rightarrow iROOH', 'RH(+M) \rightarrow CH_3+C_2H_5(+M)', 'CH_3+C_2H_5(+M) \rightarrow RH(+M)', 'nR+H \rightarrow RH', 'RH \rightarrow nR+H', 'iR+H \rightarrow RH', 'RH \rightarrow iR+H', 'RH+O_2 \rightarrow iR+HO_2', 'iR+HO_2 \rightarrow RH+O_2', 'RH+O_2 \rightarrow nR+HO_2', 'nR+HO_2 \rightarrow RH+O_2', 'H+RH \rightarrow H_2+iR', 'H_2+iR \rightarrow H+RH', 'H+RH \rightarrow H_2+nR', 'H_2+nR \rightarrow H+RH', 'RH+O \rightarrow iR+OH', 'iR+OH \rightarrow RH+O', 'RH+O \rightarrow nR+OH', 'nR+OH \rightarrow RH+O', 'RH+OH \rightarrow nR+H_2O', 'nR+H_2O \rightarrow RH+OH', 'RH+OH \rightarrow iR+H_2O', 'iR+H_2O \rightarrow RH+OH', 'RH+HO_2 \rightarrow iR+H_2O_2', 'iR+H_2O_2 \rightarrow RH+HO_2', 'RH+HO_2 \rightarrow nR+H_2O_2', 'nR+H_2O_2 \rightarrow RH+HO_2', 'CH_3+RH \rightarrow CH_4+iR', 'CH_4+iR \rightarrow CH_3+RH', 'CH_3+RH \rightarrow CH_4+nR', 'CH_4+nR \rightarrow CH_3+RH', 'iR+RH \rightarrow nR+RH', 'nR+RH \rightarrow iR+RH', 'C_2H_3+RH \rightarrow C_2H_4+iR', 'C_2H_4+iR \rightarrow C_2H_3+RH', 'C_2H_3+RH \rightarrow C_2H_4+nR', 'C_2H_4+nR \rightarrow C_2H_3+RH', 'C_2H_5+RH \rightarrow C_2H_6+iR', 'C_2H_6+iR \rightarrow C_2H_5+RH', 'C_2H_5+RH \rightarrow C_2H_6+nR', 'C_2H_6+nR \rightarrow C_2H_5+RH', 'RH+allyl \rightarrow nR+C_3H_6', 'nR+C_3H_6 \rightarrow RH+allyl', 'RH+allyl \rightarrow iR+C_3H_6', 'iR+C_3H_6 \rightarrow RH+allyl', 'RH+CH_3O \rightarrow nR+CH_3OH', 'nR+CH_3OH \rightarrow RH+CH_3O', 'RH+CH_3O \rightarrow iR+CH_3OH', 'iR+CH_3OH \rightarrow RH+CH_3O', 'CH_3OO+RH \rightarrow CH_3OOH+nR', 'CH_3OOH+nR \rightarrow CH_3OO+RH', 'CH_3OO+RH \rightarrow CH_3OOH+iR', 'CH_3OOH+iR \rightarrow CH_3OO+RH', 'CH_3CH_2OO+RH \rightarrow CH_3CH_2OOH+nR', 'CH_3CH_2OOH+nR \rightarrow CH_3CH_2OO+RH', 'CH_3CH_2OO+RH \rightarrow CH_3CH_2OOH+iR', 'CH_3CH_2OOH+iR \rightarrow CH_3CH_2OO+RH', 'RH+acetylperoxy \rightarrow iR+CH_3CO_3H', 'iR+CH_3CO_3H \rightarrow RH+acetylperoxy', 'RH+acetylperoxy \rightarrow nR+CH_3CO_3H', 'nR+CH_3CO_3H \rightarrow RH+acetylperoxy', 'RH+formylperoxy \rightarrow nR+formylooh', 'nR+formylooh \rightarrow RH+formylperoxy', 'RH+formylperoxy \rightarrow iR+formylooh', 'iR+formylooh \rightarrow RH+formylperoxy', 'H+C_3H_6 \rightarrow iR', 'iR \rightarrow H+C_3H_6', 'iR+H \rightarrow C_2H_5+CH_3', 'C_2H_5+CH_3 \rightarrow iR+H', 'iR+OH \rightarrow C_3H_6+H_2O', 'C_3H_6+H_2O \rightarrow iR+OH', 'iR+O \rightarrow acetaldehyde+CH_3', 'acetaldehyde+CH_3 \rightarrow iR+O', 'nR \rightarrow CH_3+C_2H_4', 'CH_3+C_2H_4 \rightarrow nR', 'nR \rightarrow H+C_3H_6', 'H+C_3H_6 \rightarrow nR', 'propanal+nR \rightarrow propionyl+RH', 'propionyl+RH \rightarrow propanal+nR', 'propanal+iR \rightarrow propionyl+RH', 'propionyl+RH \rightarrow propanal+iR', 'propanal+allyl \rightarrow propionyl+C_3H_6', 'propionyl+C_3H_6 \rightarrow propanal+allyl', 'allyl+H(+M) \rightarrow C_3H_6(+M)', 'C_3H_6(+M) \rightarrow allyl+H(+M)', 'C_2H_3+CH_3(+M) \rightarrow C_3H_6(+M)', 'C_3H_6(+M) \rightarrow C_2H_3+CH_3(+M)', 'C_3H_6 \rightarrow propen1yl+H', 'propen1yl+H \rightarrow C_3H_6', 'C_3H_6 \rightarrow propen2yl+H', 'propen2yl+H \rightarrow C_3H_6', 'C_3H_6+O \rightarrow C_2H_5+HCO', 'C_2H_5+HCO \rightarrow C_3H_6+O', 'C_3H_6+O \rightarrow ketene+CH_3+H', 'ketene+CH_3+H \rightarrow C_3H_6+O', 'C_3H_6+O \rightarrow CH_3CHCO+H+H', 'CH_3CHCO+H+H \rightarrow C_3H_6+O', 'C_3H_6+O \rightarrow allyl+OH', 'allyl+OH \rightarrow C_3H_6+O', 'C_3H_6+O \rightarrow propen1yl+OH', 'propen1yl+OH \rightarrow C_3H_6+O', 'C_3H_6+O \rightarrow propen2yl+OH', 'propen2yl+OH \rightarrow C_3H_6+O', 'C_3H_6+HO_2 \rightarrow allyl+H_2O_2', 'allyl+H_2O_2 \rightarrow C_3H_6+HO_2', 'C_3H_6+HO_2 \rightarrow propen1yl+H_2O_2', 'propen1yl+H_2O_2 \rightarrow C_3H_6+HO_2', 'C_3H_6+HO_2 \rightarrow propen2yl+H_2O_2', 'propen2yl+H_2O_2 \rightarrow C_3H_6+HO_2', 'C_3H_6+H \rightarrow C_2H_4+CH_3', 'C_2H_4+CH_3 \rightarrow C_3H_6+H', 'C_3H_6+H \rightarrow allyl+H_2', 'allyl+H_2 \rightarrow C_3H_6+H', 'C_3H_6+H \rightarrow propen2yl+H_2', 'propen2yl+H_2 \rightarrow C_3H_6+H', 'C_3H_6+H \rightarrow propen1yl+H_2', 'propen1yl+H_2 \rightarrow C_3H_6+H', 'C_3H_6+O_2 \rightarrow allyl+HO_2', 'allyl+HO_2 \rightarrow C_3H_6+O_2', 'C_3H_6+O_2 \rightarrow propen1yl+HO_2', 'propen1yl+HO_2 \rightarrow C_3H_6+O_2', 'C_3H_6+O_2 \rightarrow propen2yl+HO_2', 'propen2yl+HO_2 \rightarrow C_3H_6+O_2', 'C_3H_6+CH_3 \rightarrow allyl+CH_4', 'allyl+CH_4 \rightarrow C_3H_6+CH_3', 'C_3H_6+CH_3 \rightarrow propen1yl+CH_4', 'propen1yl+CH_4 \rightarrow C_3H_6+CH_3', 'C_3H_6+CH_3 \rightarrow propen2yl+CH_4', 'propen2yl+CH_4 \rightarrow C_3H_6+CH_3', 'C_3H_6+C_2H_5 \rightarrow allyl+C_2H_6', 'allyl+C_2H_6 \rightarrow C_3H_6+C_2H_5', 'C_3H_6+acetylperoxy \rightarrow allyl+CH_3CO_3H', 'allyl+CH_3CO_3H \rightarrow C_3H_6+acetylperoxy', 'C_3H_6+CH_3OO \rightarrow allyl+CH_3OOH', 'allyl+CH_3OOH \rightarrow C_3H_6+CH_3OO', 'C_3H_6+HO_2 \rightarrow propen1ol+OH', 'propen1ol+OH \rightarrow C_3H_6+HO_2', 'C_3H_6+CH_3CH_2OO \rightarrow allyl+CH_3CH_2OOH', 'allyl+CH_3CH_2OOH \rightarrow C_3H_6+CH_3CH_2OO', 'C_3H_6+nROO \rightarrow allyl+nROOH', 'allyl+nROOH \rightarrow C_3H_6+nROO', 'C_3H_6+iROO \rightarrow allyl+iROOH', 'allyl+iROOH \rightarrow C_3H_6+iROO', 'allyl+O \rightarrow acrolein+H', 'acrolein+H \rightarrow allyl+O', 'allyl+OH \rightarrow acrolein+H+H', 'acrolein+H+H \rightarrow allyl+OH', 'allyl+O_2 \rightarrow acetyl+CH_2O', 'acetyl+CH_2O \rightarrow allyl+O_2', 'allyl+O_2 \rightarrow acrolein+OH', 'acrolein+OH \rightarrow allyl+O_2', 'allyl+HCO \rightarrow C_3H_6+CO', 'C_3H_6+CO \rightarrow allyl+HCO', 'allyl \rightarrow propen2yl', 'propen2yl \rightarrow allyl', 'allyl \rightarrow propen1yl', 'propen1yl \rightarrow allyl', 'C_2H_2+CH_3 \rightarrow allyl', 'allyl \rightarrow C_2H_2+CH_3', 'allyl+CH_3OO \rightarrow allyloxy+CH_3O', 'allyloxy+CH_3O \rightarrow allyl+CH_3OO', 'allyl+C_2H_5 \rightarrow C_2H_4+C_3H_6', 'C_2H_4+C_3H_6 \rightarrow allyl+C_2H_5', 'C_2H_2+CH_3 \rightarrow propen1yl', 'propen1yl \rightarrow C_2H_2+CH_3', 'propen1yl+O \rightarrow C_2H_4+HCO', 'C_2H_4+HCO \rightarrow propen1yl+O', 'propen1yl+OH \rightarrow C_2H_4+HCO+H', 'C_2H_4+HCO+H \rightarrow propen1yl+OH', 'propen1yl+O_2 \rightarrow acetaldehyde+HCO', 'acetaldehyde+HCO \rightarrow propen1yl+O_2', 'propen1yl+HO_2 \rightarrow C_2H_4+HCO+OH', 'C_2H_4+HCO+OH \rightarrow propen1yl+HO_2', 'propen1yl+HCO \rightarrow C_3H_6+CO', 'C_3H_6+CO \rightarrow propen1yl+HCO', 'C_2H_2+CH_3 \rightarrow propen2yl', 'propen2yl \rightarrow C_2H_2+CH_3', 'propen2yl \rightarrow propen1yl', 'propen1yl \rightarrow propen2yl', 'propen2yl+O \rightarrow CH_3+ketene', 'CH_3+ketene \rightarrow propen2yl+O', 'propen2yl+OH \rightarrow CH_3+ketene+H', 'CH_3+ketene+H \rightarrow propen2yl+OH', 'propen2yl+O_2 \rightarrow acetyl+CH_2O', 'acetyl+CH_2O \rightarrow propen2yl+O_2', 'propen2yl+HO_2 \rightarrow CH_3+ketene+OH', 'CH_3+ketene+OH \rightarrow propen2yl+HO_2', 'propen2yl+HCO \rightarrow C_3H_6+CO', 'C_3H_6+CO \rightarrow propen2yl+HCO', 'nR+HO_2 \rightarrow nRO+OH', 'nRO+OH \rightarrow nR+HO_2', 'iR+HO_2 \rightarrow iRO+OH', 'iRO+OH \rightarrow iR+HO_2', 'CH_3OO+nR \rightarrow CH_3O+nRO', 'CH_3O+nRO \rightarrow CH_3OO+nR', 'CH_3OO+iR \rightarrow CH_3O+iRO', 'CH_3O+iRO \rightarrow CH_3OO+iR', 'nROO+CH_2O \rightarrow nROOH+HCO', 'nROOH+HCO \rightarrow nROO+CH_2O', 'nROO+acetaldehyde \rightarrow nROOH+acetyl', 'nROOH+acetyl \rightarrow nROO+acetaldehyde', 'iROO+CH_2O \rightarrow iROOH+HCO', 'iROOH+HCO \rightarrow iROO+CH_2O', 'iROO+acetaldehyde \rightarrow iROOH+acetyl', 'iROOH+acetyl \rightarrow iROO+acetaldehyde', 'nROO+HO_2 \rightarrow nROOH+O_2', 'nROOH+O_2 \rightarrow nROO+HO_2', 'iROO+HO_2 \rightarrow iROOH+O_2', 'iROOH+O_2 \rightarrow iROO+HO_2', 'C_2H_4+nROO \rightarrow C_2H_3+nROOH', 'C_2H_3+nROOH \rightarrow C_2H_4+nROO', 'C_2H_4+iROO \rightarrow C_2H_3+iROOH', 'C_2H_3+iROOH \rightarrow C_2H_4+iROO', 'CH_3OH+nROO \rightarrow CH_2OH+nROOH', 'CH_2OH+nROOH \rightarrow CH_3OH+nROO', 'CH_3OH+iROO \rightarrow CH_2OH+iROOH', 'CH_2OH+iROOH \rightarrow CH_3OH+iROO', 'acrolein+nROO \rightarrow CH_2CHCO+nROOH', 'CH_2CHCO+nROOH \rightarrow acrolein+nROO', 'acrolein+iROO \rightarrow CH_2CHCO+iROOH', 'CH_2CHCO+iROOH \rightarrow acrolein+iROO', 'CH_4+nROO \rightarrow CH_3+nROOH', 'CH_3+nROOH \rightarrow CH_4+nROO', 'CH_4+iROO \rightarrow CH_3+iROOH', 'CH_3+iROOH \rightarrow CH_4+iROO', 'nROO+CH_3OO \rightarrow nRO+CH_3O+O_2', 'nRO+CH_3O+O_2 \rightarrow nROO+CH_3OO', 'iROO+CH_3OO \rightarrow iRO+CH_3O+O_2', 'iRO+CH_3O+O_2 \rightarrow iROO+CH_3OO', 'H_2+nROO \rightarrow H+nROOH', 'H+nROOH \rightarrow H_2+nROO', 'H_2+iROO \rightarrow H+iROOH', 'H+iROOH \rightarrow H_2+iROO', 'iROO+C_2H_6 \rightarrow iROOH+C_2H_5', 'iROOH+C_2H_5 \rightarrow iROO+C_2H_6', 'nROO+C_2H_6 \rightarrow nROOH+C_2H_5', 'nROOH+C_2H_5 \rightarrow nROO+C_2H_6', 'iROO+propanal \rightarrow iROOH+propionyl', 'iROOH+propionyl \rightarrow iROO+propanal', 'nROO+propanal \rightarrow nROOH+propionyl', 'nROOH+propionyl \rightarrow nROO+propanal', 'iROO+acetylperoxy \rightarrow iRO+acetyloxy+O_2', 'iRO+acetyloxy+O_2 \rightarrow iROO+acetylperoxy', 'nROO+acetylperoxy \rightarrow nRO+acetyloxy+O_2', 'nRO+acetyloxy+O_2 \rightarrow nROO+acetylperoxy', 'iROO+CH_3CH_2OO \rightarrow iRO+ethoxy+O_2', 'iRO+ethoxy+O_2 \rightarrow iROO+CH_3CH_2OO', 'nROO+CH_3CH_2OO \rightarrow nRO+ethoxy+O_2', 'nRO+ethoxy+O_2 \rightarrow nROO+CH_3CH_2OO', 'iROO+iROO \rightarrow O_2+iRO+iRO', 'O_2+iRO+iRO \rightarrow iROO+iROO', 'nROO+nROO \rightarrow O_2+nRO+nRO', 'O_2+nRO+nRO \rightarrow nROO+nROO', 'iROO+nROO \rightarrow iRO+nRO+O_2', 'iRO+nRO+O_2 \rightarrow iROO+nROO', 'iROO+CH_3 \rightarrow iRO+CH_3O', 'iRO+CH_3O \rightarrow iROO+CH_3', 'iROO+C_2H_5 \rightarrow iRO+ethoxy', 'iRO+ethoxy \rightarrow iROO+C_2H_5', 'iROO+iR \rightarrow iRO+iRO', 'iRO+iRO \rightarrow iROO+iR', 'iROO+nR \rightarrow iRO+nRO', 'iRO+nRO \rightarrow iROO+nR', 'iROO+allyl \rightarrow iRO+allyloxy', 'iRO+allyloxy \rightarrow iROO+allyl', 'nROO+CH_3 \rightarrow nRO+CH_3O', 'nRO+CH_3O \rightarrow nROO+CH_3', 'nROO+C_2H_5 \rightarrow nRO+ethoxy', 'nRO+ethoxy \rightarrow nROO+C_2H_5', 'nROO+iR \rightarrow nRO+iRO', 'nRO+iRO \rightarrow nROO+iR', 'nROO+nR \rightarrow nRO+nRO', 'nRO+nRO \rightarrow nROO+nR', 'nROO+allyl \rightarrow nRO+allyloxy', 'nRO+allyloxy \rightarrow nROO+allyl', 'C_2H_5+CH_2O \rightarrow nRO', 'nRO \rightarrow C_2H_5+CH_2O', 'propanal+H \rightarrow nRO', 'nRO \rightarrow propanal+H', 'CH_3+acetaldehyde \rightarrow iRO', 'iRO \rightarrow CH_3+acetaldehyde', 'allyloxy+O_2 \rightarrow acrolein+HO_2', 'acrolein+HO_2 \rightarrow allyloxy+O_2', 'propen1ol \rightarrow C_2H_4+CH_2O', 'C_2H_4+CH_2O \rightarrow propen1ol', 'propen1ol+OH \rightarrow CH_2O+C_2H_3+H_2O', 'CH_2O+C_2H_3+H_2O \rightarrow propen1ol+OH', 'propen1ol+H \rightarrow CH_2O+C_2H_3+H_2', 'CH_2O+C_2H_3+H_2 \rightarrow propen1ol+H', 'propen1ol+O \rightarrow CH_2O+C_2H_3+OH', 'CH_2O+C_2H_3+OH \rightarrow propen1ol+O', 'propen1ol+HO_2 \rightarrow CH_2O+C_2H_3+H_2O_2', 'CH_2O+C_2H_3+H_2O_2 \rightarrow propen1ol+HO_2', 'propen1ol+CH_3OO \rightarrow CH_2O+C_2H_3+CH_3OOH', 'CH_2O+C_2H_3+CH_3OOH \rightarrow propen1ol+CH_3OO', 'propen1ol+CH_3 \rightarrow CH_2O+C_2H_3+CH_4', 'CH_2O+C_2H_3+CH_4 \rightarrow propen1ol+CH_3', 'C_3H_6+OH \rightarrow allyl+H_2O', 'allyl+H_2O \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow propen1yl+H_2O', 'propen1yl+H_2O \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow propen2yl+H_2O', 'propen2yl+H_2O \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow allyl-alcohol+H', 'allyl-alcohol+H \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow ethenol+CH_3', 'ethenol+CH_3 \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow propen1ol+H', 'propen1ol+H \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow propen2ol+H', 'propen2ol+H \rightarrow C_3H_6+OH', 'C_3H_6+OH \rightarrow acetaldehyde+CH_3', 'acetaldehyde+CH_3 \rightarrow C_3H_6+OH', 'allyl+HO_2 \rightarrow prod_2', 'prod_2 \rightarrow allyl+HO_2', 'allyl+HO_2 \rightarrow acrolein+H_2O', 'acrolein+H_2O \rightarrow allyl+HO_2', 'allyl+HO_2 \rightarrow allyloxy+OH', 'allyloxy+OH \rightarrow allyl+HO_2', 'prod_2 \rightarrow acrolein+H_2O', 'acrolein+H_2O \rightarrow prod_2', 'prod_2 \rightarrow allyloxy+OH', 'allyloxy+OH \rightarrow prod_2', 'allyloxy \rightarrow C_2H_3+CH_2O', 'C_2H_3+CH_2O \rightarrow allyloxy', 'allyloxy \rightarrow vinoxylmethyl', 'vinoxylmethyl \rightarrow allyloxy', 'allyloxy \rightarrow formylethyl', 'formylethyl \rightarrow allyloxy', 'allyloxy \rightarrow acrolein+H', 'acrolein+H \rightarrow allyloxy', 'allyloxy \rightarrow C_2H_4+HCO', 'C_2H_4+HCO \rightarrow allyloxy', 'vinoxylmethyl \rightarrow C_2H_3+CH_2O', 'C_2H_3+CH_2O \rightarrow vinoxylmethyl', 'vinoxylmethyl \rightarrow formylethyl', 'formylethyl \rightarrow vinoxylmethyl', 'vinoxylmethyl \rightarrow acrolein+H', 'acrolein+H \rightarrow vinoxylmethyl', 'vinoxylmethyl \rightarrow C_2H_4+HCO', 'C_2H_4+HCO \rightarrow vinoxylmethyl', 'formylethyl \rightarrow C_2H_3+CH_2O', 'C_2H_3+CH_2O \rightarrow formylethyl', 'formylethyl \rightarrow acrolein+H', 'acrolein+H \rightarrow formylethyl', 'formylethyl \rightarrow C_2H_4+HCO', 'C_2H_4+HCO \rightarrow formylethyl', 'C_2H_3+CH_2O \rightarrow acrolein+H', 'acrolein+H \rightarrow C_2H_3+CH_2O', 'C_2H_3+CH_2O \rightarrow C_2H_4+HCO', 'C_2H_4+HCO \rightarrow C_2H_3+CH_2O', 'O_2+nR \rightarrow nROO', 'nROO \rightarrow O_2+nR', 'O_2+nR \rightarrow QOOH_2', 'QOOH_2 \rightarrow O_2+nR', 'O_2+nR \rightarrow QOOH_1', 'QOOH_1 \rightarrow O_2+nR', 'O_2+nR \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow O_2+nR', 'O_2+nR \rightarrow OH+propoxide', 'OH+propoxide \rightarrow O_2+nR', 'nROO \rightarrow QOOH_2', 'QOOH_2 \rightarrow nROO', 'nROO \rightarrow QOOH_1', 'QOOH_1 \rightarrow nROO', 'nROO \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow nROO', 'nROO \rightarrow OH+propoxide', 'OH+propoxide \rightarrow nROO', 'QOOH_2 \rightarrow QOOH_1', 'QOOH_1 \rightarrow QOOH_2', 'QOOH_2 \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow QOOH_2', 'QOOH_2 \rightarrow OH+propoxide', 'OH+propoxide \rightarrow QOOH_2', 'QOOH_1 \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow QOOH_1', 'QOOH_1 \rightarrow OH+propoxide', 'OH+propoxide \rightarrow QOOH_1', 'O_2+iR \rightarrow iROO', 'iROO \rightarrow O_2+iR', 'O_2+iR \rightarrow QOOH_3', 'QOOH_3 \rightarrow O_2+iR', 'O_2+iR \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow O_2+iR', 'O_2+iR \rightarrow OH+propoxide', 'OH+propoxide \rightarrow O_2+iR', 'iROO \rightarrow QOOH_3', 'QOOH_3 \rightarrow iROO', 'iROO \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow iROO', 'iROO \rightarrow OH+propoxide', 'OH+propoxide \rightarrow iROO', 'QOOH_3 \rightarrow HO_2+C_3H_6', 'HO_2+C_3H_6 \rightarrow QOOH_3', 'QOOH_3 \rightarrow OH+propoxide', 'OH+propoxide \rightarrow QOOH_3', 'HO_2+C_3H_6 \rightarrow OH+propoxide', 'OH+propoxide \rightarrow HO_2+C_3H_6', 'O_2+QOOH_1 \rightarrow O_2QOOH_1', 'O_2QOOH_1 \rightarrow O_2+QOOH_1', 'O_2+QOOH_1 \rightarrow OH+OH+OQ^\primeO_1', 'OH+OH+OQ^\primeO_1 \rightarrow O_2+QOOH_1', 'O_2+QOOH_1 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow O_2+QOOH_1', 'O_2+QOOH_1 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow O_2+QOOH_1', 'O_2+QOOH_2 \rightarrow well_2', 'well_2 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow well_3', 'well_3 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow well_5', 'well_5 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow OH+OH+frag_4', 'OH+OH+frag_4 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow OH+OH+frag_5', 'OH+OH+frag_5 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow HO_2+prod_6', 'HO_2+prod_6 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow HO_2+prod_7', 'HO_2+prod_7 \rightarrow O_2+QOOH_2', 'O_2+QOOH_2 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow O_2+QOOH_2', 'O_2+QOOH_3 \rightarrow well_2', 'well_2 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow well_3', 'well_3 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow well_5', 'well_5 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow OH+OH+frag_4', 'OH+OH+frag_4 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow OH+OH+frag_5', 'OH+OH+frag_5 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow HO_2+prod_6', 'HO_2+prod_6 \rightarrow O_2+QOOH_3', 'O_2+QOOH_3 \rightarrow HO_2+prod_7', 'HO_2+prod_7 \rightarrow O_2+QOOH_3', 'O_2QOOH_1 \rightarrow OH+OQ^\primeOOH_1', 'OH+OQ^\primeOOH_1 \rightarrow O_2QOOH_1', 'O_2QOOH_1 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow O_2QOOH_1', 'O_2QOOH_1 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow O_2QOOH_1', 'well_2 \rightarrow well_3', 'well_3 \rightarrow well_2', 'well_2 \rightarrow well_5', 'well_5 \rightarrow well_2', 'well_2 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow well_2', 'well_2 \rightarrow OH+prod_4', 'OH+prod_4 \rightarrow well_2', 'well_2 \rightarrow OH+prod_5', 'OH+prod_5 \rightarrow well_2', 'well_2 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow well_2', 'well_2 \rightarrow HO_2+prod_6', 'HO_2+prod_6 \rightarrow well_2', 'well_2 \rightarrow HO_2+prod_7', 'HO_2+prod_7 \rightarrow well_2', 'well_3 \rightarrow well_5', 'well_5 \rightarrow well_3', 'well_3 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow well_3', 'well_3 \rightarrow OH+prod_4', 'OH+prod_4 \rightarrow well_3', 'well_3 \rightarrow OH+prod_5', 'OH+prod_5 \rightarrow well_3', 'well_3 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow well_3', 'well_3 \rightarrow HO_2+prod_6', 'HO_2+prod_6 \rightarrow well_3', 'well_3 \rightarrow HO_2+prod_7', 'HO_2+prod_7 \rightarrow well_3', 'well_5 \rightarrow OH+prod_3', 'OH+prod_3 \rightarrow well_5', 'well_5 \rightarrow OH+prod_4', 'OH+prod_4 \rightarrow well_5', 'well_5 \rightarrow OH+prod_5', 'OH+prod_5 \rightarrow well_5', 'well_5 \rightarrow HO_2+prod_2', 'HO_2+prod_2 \rightarrow well_5', 'well_5 \rightarrow HO_2+prod_6', 'HO_2+prod_6 \rightarrow well_5', 'well_5 \rightarrow HO_2+prod_7', 'HO_2+prod_7 \rightarrow well_5', 'prod_6 \rightarrow propen1oxy+OH', 'propen1oxy+OH \rightarrow prod_6', 'prod_7 \rightarrow propen2oxy+OH', 'propen2oxy+OH \rightarrow prod_7', 'OQ^\primeOOH_1 \rightarrow OQ^\primeO_1+OH', 'OQ^\primeO_1+OH \rightarrow OQ^\primeOOH_1', 'prod_3 \rightarrow frag_3+OH', 'frag_3+OH \rightarrow prod_3', 'prod_4 \rightarrow frag_4+OH', 'frag_4+OH \rightarrow prod_4', 'prod_5 \rightarrow frag_5+OH', 'frag_5+OH \rightarrow prod_5', 'OQ^\primeO_1 \rightarrow vinoxy+CH_2O', 'vinoxy+CH_2O \rightarrow OQ^\primeO_1', 'frag_4 \rightarrow acetyl+CH_2O', 'acetyl+CH_2O \rightarrow frag_4', 'frag_5 \rightarrow CH_3+glyoxal', 'CH_3+glyoxal \rightarrow frag_5', 'frag_5 \rightarrow HCO+acetaldehyde', 'HCO+acetaldehyde \rightarrow frag_5'};

%% figure related
% marker
% markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.', 'none'};
markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.'};
% markers = {'none'};

%% load trajectory data
%% import time
fn_time = fullfile(sohr_dir, 'output', 'time_dlsode_M.csv');
delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_time,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
time_vec = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_time delimiter formatSpec fileID dataArray ans;

%% import temperature
fn_temp = fullfile(sohr_dir, 'output', 'temperature_dlsode_M.csv');
delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_temp,'r');
%% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
%% Allocate imported array to column variable names
temp_vec = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_temp delimiter formatSpec fileID dataArray ans;

%% import reaction rate
fn_R = fullfile(sohr_dir, 'output', 'reaction_rate_dlsode_M.csv');
delimiter = ',';
formatStr = '';
for i=1:N_R
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);
fileID = fopen(fn_R,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
reaction_R_mat = [dataArray{1:end-1}];
clearvars fn_R delimiter formatSpec fileID dataArray ans;

%% calculate theta first, using equation (13)
r_idx_16 = 1162 + 1;
r_idx_14 = 1080 + 1;
theta = reaction_R_mat(:, r_idx_16) ./ reaction_R_mat(:, r_idx_14);

%% calculate alpha using equation (28)
r_idx_3 = 736 + 1;
r_idx_4 = 738 + 1;
r_idx_20 = 90 + 1;
r_idx_23 = 44 + 1;

alpha = (reaction_R_mat(:, r_idx_3)) ./ (reaction_R_mat(:, r_idx_3) ...
    + reaction_R_mat(:, r_idx_4) ...
    + reaction_R_mat(:, r_idx_20) ...
    + reaction_R_mat(:, r_idx_23));
%% calculate beta using equation (29)
r_idx_12 = 1082 + 1;
r_idx_26 = 914 + 1;
r_idx_27 = 922 + 1;

beta = (theta .*  reaction_R_mat(:, r_idx_14) ) ./ (theta .* reaction_R_mat(:, r_idx_14) ...
    + reaction_R_mat(:, r_idx_12) ...
    + reaction_R_mat(:, r_idx_26) ...
    + reaction_R_mat(:, r_idx_27));

%% read merchant f value
fn_2d_f = fullfile(pic_dir, 'species_chi_value_2d.csv');

delimiter = ',';
formatStr = '';
for i=1:n_path
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);

%% Open the text file.
fileID = fopen(fn_2d_f,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue', NaN,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
f_mat = [dataArray{1:end-1}];

t0 = f_mat(:, 1) .* tau;
tf = f_mat(:, 2) .* tau;
% tf = tf - t0;

% path index
t_offset = 2;

for i = 1:length(path_idx)
    if i==1
        f_value = f_mat(:, t_offset + path_idx(i));
    else
        f_value = f_value + f_mat(:, t_offset + path_idx(i));
    end    
end
f_interp = scatteredInterpolant(t0, tf, f_value);

% for first panel, contour plot
% construct 3d surface
xlin = linspace(min(t0), max(t0), 100);
ylin = linspace(min(tf), max(tf), 100);
[X,Y] = meshgrid(xlin, ylin);
f = scatteredInterpolant(t0, tf, f_value);
Z = f(X,Y);

%% update Z
for i = 1:length(X)    
    for j = i+1:length(X)
        Z(i,j) = nan;
    end
end

% for second panel, delta t dependent lines
for idx=1:N_plot
    % tf vector
    tf_vec = (delta_t_vec + t_value(idx)) .* tau;
    % t0 vector
    t0_vec = ones(1, length(delta_t_vec));
    t0_vec = t0_vec.* t_value(idx) * tau;
    
    Z_tmp = f_interp(t0_vec, tf_vec);
    % check data
    for i=1:length(tf_vec)
        if tf_vec(i) > str2double(end_t_sohr) * tau
            Z_tmp(i) = nan;
        end
    end
    time_cell{idx, 1} = delta_t_vec .* tau;
    yvalue_cell{idx, 1} = Z_tmp;
end

%% plot
fig = figure();
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    
%     0    0.4470    0.7410 % 1th plot
    1   0   0 % bl
    ]; 
set(fig,'defaultAxesColorOrder',co);

xpos = [0.15 0.93];
ypos = [0.095, 0.510, 0.580, 0.99];

%##########################################################################
% Panel 1
%##########################################################################
iax = 1; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

% reduce number of s.f. in coutour plot, get v_min and v_max
[c, handler] = contour(X,Y,Z, 15, 'ShowText', 'off');
% LevelList = handler.LevelList;
% v = fprintf('%1.2f, ', LevelList)
% v = [0.10,0.14,0.19,0.24,0.29,0.34,0.38,0.43,0.48,0.53];
% v = [1.11, 1.20, 1.29, 1.37, 1.46, 1.55, 1.63, 1.72, 1.81, 1.89, 1.98, 2.07, 2.15, 2.24, 2.33, 2.41, 2.50, 2.59, 2.67, 2.76];
% contour(X,Y,Z, v, 'ShowText', 'on');
contourcbar;

%% settings
set(gca,'GridLineStyle','--');
xlabel('$t$ (seconds)', 'Interpreter', 'Latex', 'FontSize', 20);
ylabel('$t+\delta$ (seconds)', 'Interpreter', 'Latex', 'FontSize', 20);

%% global settings
grid on;
xlim([0, tau*str2double(end_t_plot)]);

%% text
a_x = gca;
t_x = a_x.XLim(1) + 0.525*(a_x.XLim(2) - a_x.XLim(1));
t_y = a_x.YLim(1) + 0.278*(a_x.YLim(2) - a_x.YLim(1));
text(t_x, t_y, ['$\chi(t, t+\delta)$', newline spe_name], 'Interpreter','latex', 'FontSize', 20);
% text(t_x, t_y, ['$\chi(t, t+\delta)$', char(10) spe_name], 'Interpreter','latex', 'FontSize', 20);

%##########################################################################
% Panel 2
%##########################################################################
iax = 2; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

colors = lines(N_plot);

colorxn_idx = 1;
markerxn_idx = 1;
legend_name = cell(N_plot,1);
for idx=1:N_plot
    legend_name{idx, 1} = ['t=', num2str(t_value(idx)*tau, '%4.2f'), ' seconds'];
    
    plot(time_cell{idx, 1}, yvalue_cell{idx, 1}, ...
        'LineWidth', 2, ...
        'color', colors(mod(colorxn_idx-1, length(colors))+ 1, :), ...
        'HandleVisibility','on'); hold on;
    
    hold on;
    colorxn_idx = colorxn_idx + 1;
    markerxn_idx = markerxn_idx + 1;
end


%% settings
set(gca,'GridLineStyle','--');
xlabel('\delta (seconds)', 'FontSize', 20);
ylabel('$\chi(t, t+\delta)$', 'Interpreter', 'Latex', 'FontSize', 20);

% %% text
% a_x = gca;
% t_x = a_x.XLim(1) + 0.765*(a_x.XLim(2) - a_x.XLim(1));
% t_y = a_x.YLim(1) + 0.780*(a_x.YLim(2) - a_x.YLim(1));
% text(t_x, t_y, spe_name, ...
%     'Fontsize', 14, 'Interpreter','latex');

%% global settings
grid on;
xlim(xlim_range);
leg_h = legend(legend_name);
set(leg_h, 'FontSize', 14, 'Box', 'off');
set(leg_h, 'Location', 'South');

%% figure size
x0=10;
y0=10;
width=400;
height=600;
set(gcf,'units','points','position',[x0,y0,width,height]);

%% save to file
figname = strcat(fig_prefix, '_', spe_name, '_', num2str(sum(path_idx)), '_v1.png');
figname = strrep(figname, ' ', '_');
print(fig, fullfile(pic_dir, figname), '-r200', '-dpng');
