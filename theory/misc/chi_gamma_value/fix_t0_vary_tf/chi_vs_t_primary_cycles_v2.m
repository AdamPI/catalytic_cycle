sohr_dir = fullfile(fileparts(mfilename('fullpath')), '..', '..', '..', '..', '..', '..', '..', 'SOHR_DATA');
pic_dir = fullfile(fileparts(mfilename('fullpath')), 'nR_iR_path8');

% marker
% markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.', 'none'};
markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.'};
% markers = {'none'};

fig_prefix = 'chi_vs_t_primary_cycles';
tau = 0.777660157519;
end_t_sohr = '1.0';
end_t_plot = '0.9';
ylim_range = [0, 0.8];

n_path = 119;
nR_n_path = 63;
n_path_file = n_path + 3;

%% global propertities
% delta_t_value = 0.00001285908748611141;
% delta_t_value = 0.0001285908748611141;
% delta_t_value = 0.001285908748611141;
% delta_t_value = 0.009644315614583557;
% delta_t_value = 0.01285908748611141;
% delta_t_value = 0.09644315614583557;
delta_t_value = 0.1285908748611141;
% delta_t_value = 0.2571817497222282;
% delta_t_value = 0.38577262458334227;

time_cell = cell(n_path, 1);
yvalue_cell = cell(n_path, 1);

%% import time
fn_time = fullfile(sohr_dir, 'output', 'time_dlsode_M.csv');
delimiter3 = '';
formatSpec3 = '%f%[^\n\r]';
%% open the text file.
fileID3 = fopen(fn_time,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);

%% close the text file.
fclose(fileID3);
time_vec = dataArray3{:, 1};
%% clear temporary variables
clearvars fn_time delimiter formatSpec fileID dataArray ans;

%% import temperature
fn_temp = fullfile(sohr_dir, 'output', 'temperature_dlsode_M.csv');
delimiter3 = '';
formatSpec3 = '%f%[^\n\r]';
%% open the text file.
fileID3 = fopen(fn_temp,'r');
%% read columns of data according to format string.
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% close the text file.
fclose(fileID3);
%% allocate imported array to column variable names
temp_vec = dataArray3{:, 1};
%% clear temporary variables
clearvars fn_temp delimiter formatSpec fileID dataArray ans;

%% import reaction rate
fn_R = fullfile(sohr_dir, 'output', 'reaction_rate_dlsode_M.csv');
delimiter3 = ',';
% For more information, see the TEXTSCAN documentation.
formatSpec3 = '%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%f%[^\n\r]';

%% open the text file.
fileID3 = fopen(fn_R,'r');
dataArray3 = textscan(fileID3, formatSpec3, 'Delimiter', delimiter3, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% close the text file.
fclose(fileID3);
%% create output variable
reaction_chi_path_matrix = [dataArray3{1:end-1}];
%% clear temporary variables
clearvars fn_R delimiter formatSpec fileID dataArray ans;

%% calculate theta first, using equation (13)
r_idx_16 = 1162 + 1;
r_idx_14 = 1080 + 1;
theta13 = reaction_chi_path_matrix(:, r_idx_16) ./ reaction_chi_path_matrix(:, r_idx_14);

%% calculate alpha using equation (28)
r_idx_3 = 736 + 1;
r_idx_4 = 738 + 1;
r_idx_20 = 90 + 1;
r_idx_23 = 44 + 1;

alpha28 = (reaction_chi_path_matrix(:, r_idx_3)) ./ (reaction_chi_path_matrix(:, r_idx_3) ...
    + reaction_chi_path_matrix(:, r_idx_4) ...
    + reaction_chi_path_matrix(:, r_idx_20) ...
    + reaction_chi_path_matrix(:, r_idx_23));
%% calculate beta using equation (29)
r_idx_12 = 1082 + 1;
r_idx_26 = 914 + 1;
r_idx_27 = 922 + 1;

beta29 = (theta13 .*  reaction_chi_path_matrix(:, r_idx_14) ) ./ (theta13 .* reaction_chi_path_matrix(:, r_idx_14) ...
    + reaction_chi_path_matrix(:, r_idx_12) ...
    + reaction_chi_path_matrix(:, r_idx_26) ...
    + reaction_chi_path_matrix(:, r_idx_27));

%% read chi value, construct data structure
%% read chi value
fn_2d_chi = fullfile(pic_dir, 'species_chi_value_2d.csv');

delimiter = ',';
formatStr = '';
for i=1:n_path_file
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);

%% open the text file.
fileID = fopen(fn_2d_chi,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue', NaN,  'ReturnOnError', false);
%% close the text file.
fclose(fileID);
f_mat_chi = [dataArray{1:end-1}];

t0_chi = f_mat_chi(:, 1);
tf_chi = f_mat_chi(:, 2);
tf_chi = tf_chi - t0_chi;

% path index
offset_chi = 2;

%% data transformation, construct data
% path name
path_name_latex = cell(1, n_path);
for idx1 = 1:n_path
    path_name_latex{1, idx1} = ['P_{', num2str(idx1), '}'];
end

% nR primary cycle
for idx1=1:n_path
    path_idx = idx1;
    [time_cell{idx1, 1}, yvalue_cell{idx1, 1}] = interp_time_y_vec(end_t_sohr, delta_t_value, f_mat_chi, t0_chi, tf_chi, offset_chi, path_idx);
end

%% data clean
% check nan values
for idx1=1:n_path
    if isempty(yvalue_cell{idx1,1})
        continue
    end
    validIndices = ~isnan(yvalue_cell{idx1,1});
    yvalue_cell{idx1,1} = yvalue_cell{idx1, 1}(validIndices);
end
validSize = length(yvalue_cell{1,1});

% alpha beta
time_chi_vec = time_cell{1, 1}(1:validSize) .* tau;

alpha_chi_vec = ones(1, length(time_chi_vec));
for idx1=1:length(time_chi_vec)
    t_tmp = time_chi_vec(idx1);
    alpha_chi_vec(idx1) = interp1(time_vec, alpha28, t_tmp);
end
% in case the first value is NAN
alpha_chi_vec(1) = alpha_chi_vec(2);
% three alpha beta - 1
alpha_beta_delta_n = 800;
three_alpha_beta_vec = 3 .* alpha28 .* beta29 - 1;
three_alpha_beta_vec(1) = three_alpha_beta_vec(6);
three_alpha_beta_vec(2) = three_alpha_beta_vec(6);
three_alpha_beta_vec(3) = three_alpha_beta_vec(6);
three_alpha_beta_vec(4) = three_alpha_beta_vec(6);
three_alpha_beta_vec(5) = three_alpha_beta_vec(6);

% our beta correction 1
sink_beta_primary = Z4_sink_beta(time_vec, reaction_chi_path_matrix, [1, 2, 3], []);
% sink_beta_primary_c1 = Z4_sink_beta(time_vec, reaction_chi_path_matrix, [1, 2, 3], [8, 9]);
% sink_beta_primary_c2 = Z4_sink_beta(time_vec, reaction_chi_path_matrix, [1, 2, 3], [4, 5, 8, 9]);
sink_beta_primary_c1 = Z4_sink_beta(time_vec, reaction_chi_path_matrix, [1, 2, 3], [8]);
sink_beta_primary_c2 = Z4_sink_beta(time_vec, reaction_chi_path_matrix, [1, 2, 3], [4, 5, 8]);
sink_beta_correction1 = sink_beta_primary_c1 ./ sink_beta_primary;
sink_beta_correction2 = sink_beta_primary_c2 ./ sink_beta_primary;

%% construct vector and matrix for future usage
time_vec_chi = time_cell{1, 1};
chi_path_matrix = zeros(length(yvalue_cell), length(yvalue_cell{1,1}));
for row_i=1:length(yvalue_cell)
    for col_j=1:length(yvalue_cell{row_i,1})
        chi_path_matrix(row_i, col_j) = yvalue_cell{row_i, 1}(col_j);
    end
end

% multiply by alpha
for idx1=1:nR_n_path
    chi_path_matrix(idx1, :) = chi_path_matrix(idx1, :) .* alpha_chi_vec;
end
for idx1=nR_n_path+1:n_path
    chi_path_matrix(idx1, :) = chi_path_matrix(idx1, :) .* (1 - alpha_chi_vec);
end

%% sort by the reaction rates around 0.5 tau, idx == 3550 for example
% sort_axis = round(0.1 * length(time_vec_chi));
sort_axis = round(0.5 * length(time_vec_chi));
% sort_axis = round(0.675 * length(time_vec_chi));

% source reactions
target_array = linspace(1, n_path, n_path);
number_array = ones(1, n_path);
[B,I] = sort(chi_path_matrix(target_array, sort_axis),'descend');
% old index to new index
old_2_new_I = ones(length(I), 1);
for idx=1:length(old_2_new_I)
    old_2_new_I(I(idx)) = idx;
end

topN_array_in_old_I = {[2,1,3], ...
    75, ...
    7, ...
    65, ...
    35, ...
    [70,71,72], ...
%     [82,83,81], ...
%     [51,52,50]
    };
% known topN_array_in_old_I, calculate topN_array_in_new_I
topN_array_in_new_I = cell(1,size(topN_array_in_old_I,2));
for idx1=1:size(topN_array_in_old_I,2)
    topN_array_in_new_I{1, idx1} = ones(1, length(topN_array_in_old_I{1, idx1}));
    for idx2=1:length(topN_array_in_old_I{1, idx1})
        topN_array_in_new_I{1, idx1}(idx2) = old_2_new_I(topN_array_in_old_I{1, idx1}(idx2));
    end
end

% topN_array_in_new_I = {[1, 2, 3], ...
%     [6, 9, 12, 13, 19, 20, 21, 27, 28, 31, 32, 44, 48, 55, 62, 67, 69, 73, 74, 83, 91, 101], ...
%     [5, 16, 17, 30, 45, 64, 81], ...
%     [4, 22, 23, 42, 47, 51, 52, 65, 82, 87], ...
%     [7, 14, 15, 29, 37, 40, 41, 86], ...
%     [33, 34, 35, 98, 105, 110], ...
%     [70, 71, 72], ...
%     [59, 60, 61]};

% known topN_array_in_new_I, calculate topN_array_in_old_I
% topN_array_in_old_I = cell(1,size(topN_array_in_new_I,2));
% for idx1=1:size(topN_array_in_new_I,2)
%     topN_array_in_old_I{1, idx1} = ones(1, length(topN_array_in_new_I{1, idx1}));
%     for idx2=1:length(topN_array_in_new_I{1, idx1})
%         topN_array_in_old_I{1, idx1}(idx2) = I(topN_array_in_new_I{1, idx1}(idx2));
%     end
% end

%% plot
fig = figure();
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    
%     0    0.4470    0.7410 % 1th plot
    1   0   0 % bl
    ]; 
set(fig,'defaultAxesColorOrder',co);

chi_N_plot = 3+1+1;
chi_colors = [
    [1, 0, 0]
    [0.3010, 0.7450, 0.9330]
    [0.4940, 0.1840, 0.5560]
    [0.9290, 0.6940, 0.1250]
    ];

chi_colorxn_idx = 1;
chi_markerxn_idx = 1;
chi_delta_n = 5;
chi_legend_name = cell(chi_N_plot - 1,1);
chi_legend_name{1, 1} = 'primary';
chi_legend_name{2, 1} = '+ nROO+RH\rightarrownROOH+iR';
chi_legend_name{3, 1} = '+ nROO+(i)nROO\rightarrowO_2+nRO+(i)nRO';
chi_legend_name{4, 1} = 'primary approx';


for idx1 = 1:1
    % plot chi*alpha - 1   
    % temporary top N array --> tNa
    tNa_tmp = topN_array_in_new_I{1, idx1};
    for idx2=1:length(tNa_tmp)
        path_idx = target_array(I(tNa_tmp(idx2)));
        if idx2 == 1
            chi_alpha_3_vec = chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
        else
            chi_alpha_3_vec = chi_alpha_3_vec + chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
        end       
    end

    chi_alpha_3_vec_minus_1 = chi_alpha_3_vec - 1;

    plot(time_chi_vec, chi_alpha_3_vec_minus_1, ...
        'LineWidth', 2, ...
        'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
        'HandleVisibility','off'); hold on;
    scatter(time_chi_vec(1:chi_delta_n:end), chi_alpha_3_vec_minus_1(1:chi_delta_n:end), ...
    'LineWidth', 2, ...
    'MarkerEdgeColor', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1}, ...
    'HandleVisibility','off'); hold on;
    plot(nan, nan, 'LineWidth', 2, 'LineStyle', '-', ...
    'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

    hold on;
    chi_colorxn_idx = chi_colorxn_idx + 1;
    chi_markerxn_idx = chi_markerxn_idx + 1;
end

% correction 1
for idx1 = 1:1
    % plot chi*alpha - 1   
    % temporary top N array --> tNa
    tNa_tmp = topN_array_in_new_I{1, idx1};
    for idx2=1:length(tNa_tmp)
        path_idx = target_array(I(tNa_tmp(idx2)));
        if idx2 == 1
            chi_alpha_3_vec = chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
        else
            chi_alpha_3_vec = chi_alpha_3_vec + chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
        end       
    end
    
    % impose correction here    
    sink_beta_correction1_for_chi = ones(1, length(chi_alpha_3_vec));
    for idx3=1:length(sink_beta_correction1_for_chi)
        sink_beta_correction1_for_chi(idx3) = interp1(time_vec(2:end), sink_beta_correction1(2:end), time_chi_vec(idx3));
    end
    % manually smooth
    sink_beta_correction1_for_chi(1) = sink_beta_correction1_for_chi(2);
    chi_alpha_3_vec_minus_1 = chi_alpha_3_vec .* sink_beta_correction1_for_chi - 1;

    plot(time_chi_vec, chi_alpha_3_vec_minus_1, ...
        'LineWidth', 2, ...
        'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
        'HandleVisibility','off'); hold on;
    scatter(time_chi_vec(1:chi_delta_n:end), chi_alpha_3_vec_minus_1(1:chi_delta_n:end), ...
    'LineWidth', 2, ...
    'MarkerEdgeColor', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1}, ...
    'HandleVisibility','off'); hold on;
    plot(nan, nan, 'LineWidth', 2, 'LineStyle', '-', ...
    'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

    hold on;
    chi_colorxn_idx = chi_colorxn_idx + 1;
    chi_markerxn_idx = chi_markerxn_idx + 1;
end

% correction 2
for idx1 = 1:1
    % plot chi*alpha - 1   
    % temporary top N array --> tNa
    tNa_tmp = topN_array_in_new_I{1, idx1};
    for idx2=1:length(tNa_tmp)
        path_idx = target_array(I(tNa_tmp(idx2)));
        if idx2 == 1
            chi_alpha_3_vec = chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
        else
            chi_alpha_3_vec = chi_alpha_3_vec + chi_path_matrix(path_idx, :) * number_array(I(tNa_tmp(idx2)));
        end       
    end
    
    % impose correction here    
    sink_beta_correction1_for_chi = ones(1, length(chi_alpha_3_vec));
    for idx3=1:length(sink_beta_correction1_for_chi)
        sink_beta_correction1_for_chi(idx3) = interp1(time_vec(2:end), sink_beta_correction2(2:end), time_chi_vec(idx3));
    end
    % manually smooth
    sink_beta_correction1_for_chi(1) = sink_beta_correction1_for_chi(2);
    chi_alpha_3_vec_minus_1 = chi_alpha_3_vec .* sink_beta_correction1_for_chi - 1;

    plot(time_chi_vec, chi_alpha_3_vec_minus_1, ...
        'LineWidth', 2, ...
        'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
        'HandleVisibility','off'); hold on;
    scatter(time_chi_vec(1:chi_delta_n:end), chi_alpha_3_vec_minus_1(1:chi_delta_n:end), ...
    'LineWidth', 2, ...
    'MarkerEdgeColor', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1}, ...
    'HandleVisibility','off'); hold on;
    plot(nan, nan, 'LineWidth', 2, 'LineStyle', '-', ...
    'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

    hold on;
    chi_colorxn_idx = chi_colorxn_idx + 1;
    chi_markerxn_idx = chi_markerxn_idx + 1;
end

% three alpha beta
plot(time_vec, three_alpha_beta_vec, ...
    'LineWidth', 2, ...
    'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
    'HandleVisibility','off'); hold on;
scatter(time_vec(1:alpha_beta_delta_n:end), three_alpha_beta_vec(1:alpha_beta_delta_n:end), ...
'LineWidth', 2, ...
'MarkerEdgeColor', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1}, ...
'HandleVisibility','off'); hold on;
plot(nan, nan, 'LineWidth', 2, 'LineStyle', '-', ...
'color', chi_colors(mod(chi_colorxn_idx-1, length(chi_colors))+ 1, :), ...
'marker', markers{1, mod(chi_markerxn_idx-1, length(markers))+ 1});

hold on;

%% settings
grid on;
set(gca,'GridLineStyle','--');
xlim([0, str2double(end_t_plot) * tau]);
% ylim([0, 1.5]);

get(gca, 'XTick');
set(gca, 'FontSize', 14);

xlabel('$t$ (seconds)', 'Interpreter','latex', 'FontSize', 24);
% xticklabels([]);
% yticks([-0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4]);
% yticklabels({'', '-0.6', '-0.4', '-0.2', '0', '0.2', '0.4'});

ylabel('$\gamma$', 'Interpreter','latex', 'FontSize', 20);

%  legend
leg_h = legend(chi_legend_name);
set(leg_h, 'FontSize', 14, 'Box', 'off');
set(leg_h, 'Location', 'SouthWest');

% vertical line
x1 = 0.4;
x2 = 0.4;
% chi_alpha_3_vec_minus_1 = chi_path_matrix(1, :)+chi_path_matrix(2, :)+chi_path_matrix(3, :) - 1;
y1 = interp1(time_chi_vec, chi_alpha_3_vec_minus_1, x1);
y2 = interp1(time_vec, three_alpha_beta_vec, x2);

h_a1 = annotation('arrow');
% https://www.mathworks.com/help/matlab/ref/matlab.graphics.shape.arrow-properties.html
set(h_a1, 'parent', gca, ...
    'position', [x1, y1,x2 - x1,y2 - y1], ...
    'HeadLength', 10, 'HeadWidth', 5, 'HeadStyle', 'vback2', ...
    'color', 'r', ...
    'LineWidth', 1.0, ...
    'LineStyle', '-');
h_a2 = annotation('arrow');
% https://www.mathworks.com/help/matlab/ref/matlab.graphics.shape.arrow-properties.html
set(h_a2, 'parent', gca, ...
    'position', [x2, y2,x2 - x1,y1 - y2], ...
    'HeadLength', 10, 'HeadWidth', 5, 'HeadStyle', 'vback2', ...
    'color', 'r', ...
    'LineWidth', 1.0, ...
    'LineStyle', '-');

% text
a_x = gca;
t_x = a_x.XLim(1) + 0.610*(a_x.XLim(2) - a_x.XLim(1));
t_y = a_x.YLim(1) + 0.875*(a_x.YLim(2) - a_x.YLim(1));
text(t_x, t_y, 'dynamical effect', ...
    'FontSize', 15, ...
    'color', 'r');

% arrow with text
h_a3 = annotation('arrow', [0.585 0.69], [0.67 0.78], 'color', 'r');

% %% figure size
% x0=10;
% y0=10;
% width=400;
% height=600;
% set(gcf,'units','points','position',[x0,y0,width,height]);

%% save to file
figname = strcat(fig_prefix, '_', end_t_sohr, '_', num2str(delta_t_value*tau), '_v2.png');
print(fig, fullfile(pic_dir, figname), '-r200', '-dpng');

%% local function definition
function [time_v_local, y_v_local] = interp_time_y_vec(end_t_local, delta_t_value_local, f_mat_local, t0_local, tf_local, offset_local, path_idx_local)
% MYMEAN Example of a local function.
    for i = 1:length(path_idx_local)
        if i==1
            f_value_local = f_mat_local(:, offset_local + path_idx_local(i));
        else
            f_value_local = f_value_local + f_mat_local(:, offset_local + path_idx_local(i));
        end    
    end
    % construct 3d surface
    xlin_local = linspace(min(t0_local), max(t0_local), 25);
    ylin_local = linspace(min(tf_local), max(tf_local), 25);
    [X_local,Y_local] = meshgrid(xlin_local, ylin_local);
    f_local = scatteredInterpolant(t0_local, tf_local, f_value_local);
    Z_local = f_local(X_local,Y_local);
    %% update Z
    for i = 1:length(X_local)    
        for j = length(X_local) - i + 1 : length(X_local)
            Z_local(i,j) = nan;
        end
    end
    % delta
    time_v_local = X_local(1, :);
    delta_t_local = ones(1, length(time_v_local));
    delta_t_local = delta_t_local.* delta_t_value_local;
    y_v_local = f_local(time_v_local, delta_t_local);
    % check data
    for i=1:length(time_v_local)
        if time_v_local(i) + delta_t_local(i) > str2double(end_t_local)
            y_v_local(i) = nan;
        end
    end

end

function sink_beta = Z4_sink_beta(time_vec, R_mat, primary_idx_in_I, exclude_idx_in_I)
% sort by the reaction rates around 0.5 tau, idx == 3550 for example
    sort_axis = round(0.42 * length(time_vec));
    target_array = [706, 709, 710, 720, 727, 731, 735, 737, 743, 747, 749, 753, 757, 759, 763, 767, 771, 777, 779, 790, 792, 794, 856, 906, 910, 914, 916, 922, 926, 930, 934, 938, 942, 946, 952, 956, 960, 964, 968, 970, 978, 982, 984, 986, 988, 990, 1070, 1074, 1076, 1078, 1082, 1084, 1087, 1092, 1094, 1118, 1120, 1122, 1162, 1164, 1166];
    target_array = target_array + 1;
    number_array = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
    % sort before add sink number from number array
    [~,I] = sort(R_mat(sort_axis, target_array),'descend');

    %% a few reaction's rate are too small, just noise
    time_threshold = round(length(time_vec)*0.1);
    NReaction_threshold = round(length(I)*0.8);
    % first/largest rate reaction
    R_total = R_mat(:, target_array(I(1))) * number_array(I(1));  

    for t_idx=1:length(time_vec)
        % short time, use about 5-10 reactions
        if t_idx < time_threshold
            NReaction_end = NReaction_threshold;
        % long time, use all reactions
        else
            NReaction_end = length(I);
        end
        for idx1=2:NReaction_end
            rxn_idx = target_array(I(idx1));
            R_total(t_idx) = (R_total(t_idx) + R_mat(t_idx, rxn_idx) * number_array(I(idx1)));
        end
    end
    
    %% manually smooth the first few points
%     R_total(1:10) = nan;
    
    for idx=1:length(primary_idx_in_I)
        rxn_idx = target_array(I(primary_idx_in_I(idx)));
        if idx == 1
            R_tmp = R_mat(:, rxn_idx) * number_array(I(primary_idx_in_I(idx)));
        else
            R_tmp = R_tmp + R_mat(:, rxn_idx) * number_array(I(primary_idx_in_I(idx)));
        end
    end
    if isempty(exclude_idx_in_I)
        R_tmp2 = zeros(length(R_total), 1);
    else
        for idx=1:length(exclude_idx_in_I)
            rxn_idx = target_array(I(exclude_idx_in_I(idx)));
            if idx == 1
                R_tmp2 = R_mat(:, rxn_idx) * number_array(I(exclude_idx_in_I(idx)));   
            else
                R_tmp2 = R_tmp2 + R_mat(:, rxn_idx) * number_array(I(exclude_idx_in_I(idx)));   
            end        
        end
    end
    
    sink_beta = R_tmp ./ (R_total - R_tmp2);
    
end
