%% current working directory
curr_dir = fullfile(fileparts(mfilename('fullpath')));
sohr_dir = fullfile(fileparts(mfilename('fullpath')), '..', '..', '..', '..', '..', '..', '..', 'SOHR_DATA');
pic_dir = fullfile(fileparts(mfilename('fullpath')));

%% global settings
fig_prefix = 'chi_value_two_panels';
N_S = 110;
N_R = 1230;

tau = 0.777660157519;
end_t = 0.9;

%% number of time points% marker
% markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.', 'none'};
markers = {'+' , 'o' , '*' , 'x' , 'square' , 'diamond' , 'v' , '^' , '>' , '<' , 'pentagram' , 'hexagram' , '.'};
% markers = {'none'};

%% load trajectory data
%% import time
fn_time = fullfile(sohr_dir, 'output', 'time_dlsode_M.csv');
delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_time,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
traj_time_vec = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_time delimiter formatSpec fileID dataArray ans;

%% import temperature
fn_temp = fullfile(sohr_dir, 'output', 'temperature_dlsode_M.csv');
delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_temp,'r');
%% Read columns of data according to format string.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
%% Allocate imported array to column variable names
temp_vec = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_temp delimiter formatSpec fileID dataArray ans;

%% import reaction rate
fn_R = fullfile(sohr_dir, 'output', 'reaction_rate_dlsode_M.csv');
delimiter = ',';
formatStr = '';
for i=1:N_R
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);
fileID = fopen(fn_R,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
reaction_R_mat = [dataArray{1:end-1}];
clearvars fn_R delimiter formatSpec fileID dataArray ans;

%% calculate theta first, using equation (13)
r_idx_16 = 1162 + 1;
r_idx_14 = 1080 + 1;
theta = reaction_R_mat(:, r_idx_16) ./ reaction_R_mat(:, r_idx_14);

%% calculate alpha using equation (28)
r_idx_3 = 736 + 1;
r_idx_4 = 738 + 1;
r_idx_20 = 90 + 1;
r_idx_23 = 44 + 1;

alpha = (reaction_R_mat(:, r_idx_3)) ./ (reaction_R_mat(:, r_idx_3) ...
    + reaction_R_mat(:, r_idx_4) ...
    + reaction_R_mat(:, r_idx_20) ...
    + reaction_R_mat(:, r_idx_23));
%% calculate beta using equation (29)
r_idx_12 = 1082 + 1;
r_idx_26 = 914 + 1;
r_idx_27 = 922 + 1;

beta = (theta .*  reaction_R_mat(:, r_idx_14) ) ./ (theta .* reaction_R_mat(:, r_idx_14) ...
    + reaction_R_mat(:, r_idx_12) ...
    + reaction_R_mat(:, r_idx_26) ...
    + reaction_R_mat(:, r_idx_27));
n_points = 25;

%% sohr time
fn_time = fullfile(curr_dir, ['pathway_time_candidate_S10_HA4_', num2str(end_t) ,'.csv']);
delimiter = ',';
formatStr = '';
for i=1:n_points
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);

fileID = fopen(fn_time,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
fclose(fileID);
time_mat = [dataArray{1:end-1}];
time_vec = time_mat(1, :);
clearvars fn_time delimiter formatSpec fileID dataArray ans time_mat;

%% pathwap probability
fn_pp = fullfile(curr_dir, ['pathway_prob_S10_HA4_', num2str(end_t) ,'.csv']);

delimiter = ',';
formatStr = '';
for i=1:n_points
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);

fileID = fopen(fn_pp,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
fclose(fileID);
pp_mat = [dataArray{1:end-1}];
clearvars fn_pp delimiter formatSpec fileID dataArray ans;

%% total top 4-1000 path
top_4_to_1000_idx = linspace(4, 1000, 1000 - 3);
for idx=1:length(top_4_to_1000_idx)
    true_idx = top_4_to_1000_idx(idx);
    if idx == 1
        top_4_to_1000_vec = pp_mat(true_idx, :);
    else
        top_4_to_1000_vec = (top_4_to_1000_vec + pp_mat(true_idx, :));
    end
end

%% plot data preperation
N_plot = 4;
colors = lines(N_plot);
colorxn_idx = 1;
markerxn_idx = 1;
delta_n = 800;

data_y_cell = cell(N_plot, 1);
data_y_cell{1, 1} = pp_mat(1, :);
data_y_cell{2, 1} = pp_mat(2, :);
data_y_cell{3, 1} = pp_mat(3, :);
data_y_cell{4, 1} = top_4_to_1000_vec;

legend_name = cell(N_plot, 1);
legend_name{1, 1} = '\chi_1';
legend_name{2, 1} = '\chi_2';
legend_name{3, 1} = '\chi_3';
legend_name{4, 1} = '\chi_{nR spur}';

%% plot
fig = figure();
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    
%     0    0.4470    0.7410 % 1th plot
    1   0   0 % bl
    ]; 
set(fig,'defaultAxesColorOrder',co)
%% plot
for idx=1:N_plot
    data_y_local = data_y_cell{idx, 1};
    for idx2 = 1:length(data_y_local)
        if isnan(data_y_local(idx2))
            data_y_local(idx2) = 0;
        end
    end

    if idx == 1
        polygon_x = [0 time_vec time_vec(end)];
        % temperary data y
        data_y_high = data_y_local;
        for idx3 = 1:length(data_y_high)
            if isnan(data_y_high(idx3))
                data_y_high(idx3) = 0;
            end
        end
        polygon_y = [0 data_y_high 0];
    else
        polygon_x = [time_vec fliplr(time_vec)];
        data_y_low = data_y_high;
        data_y_high = data_y_low + data_y_local;
        for idx3 = 1:length(data_y_high)
            if isnan(data_y_high(idx3))
                data_y_high(idx3) = 0;
            end
        end
        polygon_y = [data_y_high fliplr(data_y_low)];
    end
	
	fill(polygon_x, polygon_y, colors(mod(colorxn_idx-1, length(colors))+ 1, :), ...
	'EdgeColor', colors(mod(colorxn_idx-1, length(colors))+ 1, :), ...
	'FaceAlpha', 0.9, 'EdgeAlpha', 0.9);
    
    hold on;
    colorxn_idx = colorxn_idx + 1;
    markerxn_idx = markerxn_idx + 1;
end

%% settings
set(gca,'GridLineStyle','--');
xlabel('$t$ (seconds)', 'Interpreter','latex', 'FontSize', 20);
ylabel('$\chi$', 'Interpreter','latex', 'FontSize', 20);
% ylim([0.15, 1.8]);

%% global settings
grid on;
xlim([0, tau*end_t]);
leg_h = legend(legend_name);
set(leg_h, 'FontSize', 10, 'Box', 'off');
% set(leg_h, 'Location', 'North');

%% text
% a_x = gca;
% t_x = a_x.XLim(1) + 0.325*(a_x.XLim(2) - a_x.XLim(1));
% t_y = a_x.YLim(1) + 0.858*(a_x.YLim(2) - a_x.YLim(1));
% text(t_x, t_y, '$t_f$=$0.9\tau$', 'Interpreter','latex', 'FontSize', 20);

%% save to file
figname = strcat(fig_prefix, '_', num2str(end_t), '_fill_v1.png');
print(fig, fullfile(pic_dir, figname), '-r200', '-dpng');



