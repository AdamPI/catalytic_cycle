%% global settings
%% global settings
fig_prefix = 'sink_rxn_ratio';
N_S = 110;
N_R = 1230;
tau = '0.777660157519';

spe_idx=14;
spe_name='CO';
pre_factor = 7.44277901848944108e-06 * 3.0;
% spe_idx=17;
% spe_name='CH_2O';
% pre_factor = 7.44277901848944108e-06 * 3.0;
% spe_idx=59;
% spe_name='C_3H_6';

%% settings for three panels
% panel --> 1
folder1 = '0.2seconds';
end_t1 = '0.25718313951098054';
n_path1 = '100';
species_path1 = '';
path_type1 = 'primitive';
end_path1_idx1 = 3;
n1_delta1 = 1;
end_path1_idx2 = 39;
n1_delta2 = 1;
% panels --> 2
folder2 = '0.5tau';
end_t2 = '0.5';
n_path2 = '500';
species_path2 = '';
path_type2 = 'primitive';
end_path2_idx1 = 35;
n2_delta1 = 1;
end_path2_idx2 = 50;
n2_delta2 = 1;
% panels --> 3
folder3 = '0.5tau2';
end_t3 = '0.5';
n_path3 = '50';
species_path3 = 'species_';
path_type3 = 'merged';
end_path3_idx1 = 9;
n3_delta1 = 1;
end_path3_idx2 = 50;
n3_delta2 = 1;

%% Current file directory
file_dir = fullfile(fileparts(mfilename('fullpath')));
sohr_dir = fullfile(fileparts(mfilename('fullpath')), '..', '..', '..', '..', '..', '..', 'SOHR_DATA');

%% %% import time
fn_time = fullfile(sohr_dir, 'output', 'time_dlsode_M.csv');
delimiter = '';
formatSpec = '%f%[^\n\r]';
fileID = fopen(fn_time,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
time_vec = dataArray{:, 1};
clearvars fn_time delimiter formatSpec fileID dataArray ans;

%% import concentration
fn_conc = fullfile(sohr_dir, 'output', 'concentration_dlsode_M.csv');
delimiter = ',';
formatStr = '';
for i=1:N_S
    formatStr = strcat(formatStr, '%f');
end
formatStr = strcat(formatStr, '%[^\n\r]');
formatSpec = char(formatStr);
fileID = fopen(fn_conc,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN, 'ReturnOnError', false);
fclose(fileID);
conc_mat = [dataArray{1:end-1}];
clearvars fn_conc delimiter formatSpec fileID dataArray ans;

%% figure
fig = figure();
n_xaxis = 3;
x_ratio = 1.0;
y_ratio = 1.0/3.0;

xpos = [0.14 0.87];
vertical_space = 0.045;
ypos = [0.07, 0.3733-vertical_space, 0.3733, 0.6767-vertical_space, 0.6767, 0.96];

%##########################################################################
% Panel 1
%##########################################################################
iax = 1; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

%% sorted pathway probabilities
fn_path1_p1 = fullfile(file_dir, folder1, strcat(species_path1, 'path_prob_top_n_sorted_', ... 
    n_path1, '_',num2str(spe_idx), '_', tau, '_', end_t1, '.csv'));

delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_path1_p1,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
path_p_vec1 = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_path_p1 delimiter formatSpec fileID dataArray ans;

% concentration of a species
const_c1 = interp1(time_vec, conc_mat(:, spe_idx + 1), str2double(end_t1) * str2double(tau));

%% convert pathway probability to concentration
path_p_2_X_vec1 = path_p_vec1 * pre_factor;

%% cumulative pathway probabilities
cumu_path_p_vec1 = path_p_2_X_vec1;
for i = 2:length(cumu_path_p_vec1)
    cumu_path_p_vec1(i) = cumu_path_p_vec1(i) + cumu_path_p_vec1(i-1);
end

%% linear array
path_idx = linspace(1, length(path_p_2_X_vec1), length(path_p_2_X_vec1));
path_idx = path_idx';

%% plot
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    0    0.4470    0.7410 % 1th plot
%     0.8500    0.3250    0.0980 % 2nd plot
    0.9290    0.6940    0.1250 % 3rd plot
%     0.4940    0.1840    0.5560 % 4th plot
%     0.4660    0.6740    0.1880 % 5th plot
%     0.3010    0.7450    0.9330 % 6th plot
%     0.6350    0.0780    0.1840 % 7th plot
%     0         0    1.0000 % 8th plot, blue
    1   0   0 % for placeholder
    1   0   0]; % 9th plot, red
set(fig,'defaultAxesColorOrder',co);

% plot pathway probabilities
% % individual
% plot([path_idx(1), path_idx(end_path1_idx1)], [const_c1, const_c1], ...
%     'color',co(1, :), 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
% cumulative
plot(path_idx(1:n1_delta1:end_path1_idx1), cumu_path_p_vec1(1:n1_delta1:end_path1_idx1) , ...
    'color',co(2, :), 'linestyle', '-', 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
% placeholder for legend
plot(NaN, NaN, 'LineStyle', ':', 'LineWidth', 2.5, 'color', co(3, :), 'HandleVisibility','on');
%% concentration
% only integer xticks
xticks = get(gca, 'xtick');
xticks = xticks(mod(xticks, 1) == 0);
set(gca,'xtick', xticks);

set(gca,'GridLineStyle','--');
% primarily for tick label size
set(gca, 'FontSize', 12);
% xlabel('Number of Path', 'FontSize', 20);
ylabel('[X] (mole\cdotcm^{-3})', 'FontSize', 20);
% ylim([10^(1.0*log10(cumu_path_p_vec1(1))), 10^(0.9995*(log10(const_c1)))]);

yyaxis right;
error_data1 = (const_c1 - cumu_path_p_vec1)./const_c1.*100;
semilogy(path_idx(1:n1_delta1:end_path1_idx1), error_data1(1:n1_delta1:end_path1_idx1), ... 
    'LineStyle', ':', 'LineWidth', 2.5, 'color', co(3, :)); hold on;
ylabel('%Relative Error', 'FontSize', 20);
% set(gca, 'ytick', []);

%% global settings
grid on;
xlim([1, end_path1_idx1]);
leg_h = legend({'SOHR', 'Percentage Error'});
set(leg_h, 'FontSize', 14, 'Box', 'off');
% [left, bottom, weight, height]
set(leg_h, 'Position', [0.125*x_ratio, (0.105+n_xaxis-iax)*y_ratio, 0.5*x_ratio, 0.1*y_ratio]);

a_x = gca;
t_x = a_x.XLim(1) + 0.265*(a_x.XLim(2) - a_x.XLim(1));
t_y = a_x.YLim(1) + 0.818*(a_x.YLim(2) - a_x.YLim(1));
text(t_x, t_y, [spe_name, ', ', path_type1, ' pathways', ...
    newline 'time = ', num2str(str2double(end_t1)*str2double(tau), '%4.2f'), ' seconds'], ...
    'Fontsize', 14);

%% Zoom in figure2
co = [
    0   0   0
    0.4940    0.1840    0.5560 % 4th plot
];
set(fig,'defaultAxesColorOrder',co);
z2_position = [.515*x_ratio (.345+n_xaxis-iax)*y_ratio .255*x_ratio .25*y_ratio];
% create a new pair of axes inside current figure
axes('position',z2_position);

set(gca, 'ytick', []);
yyaxis right;
box on; % put box around new pair of axes
% pathway probability
semilogy(path_idx(1:n1_delta2:end_path1_idx2), path_p_vec1(1:n1_delta2:end_path1_idx2) , ...
    'color',co(2, :), 'linestyle', '-.', 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
tick_pos = linspace(ceil(log10(path_p_vec1(end_path1_idx2))),floor(log10(path_p_vec1(1))), ...
    1+floor(log10(path_p_vec1(1))) - ceil(log10(path_p_vec1(end_path1_idx2))));
tick_pos = arrayfun(@(x) 10^x, tick_pos);
set(gca, 'ytick', tick_pos);
axis tight;
grid on;
% legend
leg_z2 = legend(['PATHWAY' newline 'PROBABILITY']);
set(leg_z2, 'FontSize', 8, 'Box', 'off');
% [left, bottom, weight, height]
set(leg_z2, 'Position', [z2_position(1) + z2_position(3)*0.375, ...
    z2_position(2) + z2_position(4)*0.5, 0.1*x_ratio, 0.1*y_ratio]);

%##########################################################################
% Panel 2
%##########################################################################
iax = 2; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

%% sorted pathway probabilities
fn_path1_p2 = fullfile(file_dir, folder2, strcat(species_path2, 'path_prob_top_n_sorted_', ... 
    n_path2, '_',num2str(spe_idx), '_', tau, '_', end_t2, '.csv'));

delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_path1_p2,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
path_p_vec1 = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_path_p1 delimiter formatSpec fileID dataArray ans;

% concentration of a species
const_c1 = interp1(time_vec, conc_mat(:, spe_idx + 1), str2double(end_t2) * str2double(tau));

%% convert pathway probability to concentration
path_p_2_X_vec1 = path_p_vec1 * pre_factor;

%% cumulative pathway probabilities
cumu_path_p_vec1 = path_p_2_X_vec1;
for i = 2:length(cumu_path_p_vec1)
    cumu_path_p_vec1(i) = cumu_path_p_vec1(i) + cumu_path_p_vec1(i-1);
end

%% linear array
path_idx = linspace(1, length(path_p_2_X_vec1), length(path_p_2_X_vec1));
path_idx = path_idx';

%% plot
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    0    0.4470    0.7410 % 1th plot
%     0.8500    0.3250    0.0980 % 2nd plot
    0.9290    0.6940    0.1250 % 3rd plot
%     0.4940    0.1840    0.5560 % 4th plot
%     0.4660    0.6740    0.1880 % 5th plot
%     0.3010    0.7450    0.9330 % 6th plot
%     0.6350    0.0780    0.1840 % 7th plot
%     0         0    1.0000 % 8th plot, blue
    1   0   0 % for placeholder
    1   0   0]; % 9th plot, red
set(fig,'defaultAxesColorOrder',co)

% plot pathway probabilities
% % individual
% plot([path_idx(1), path_idx(end_path2_idx1)], [const_c1, const_c1], ...
%     'color',co(1, :), 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
% cumulative
plot(path_idx(1:n2_delta1:end_path2_idx1), cumu_path_p_vec1(1:n2_delta1:end_path2_idx1) , ...
    'color',co(2, :), 'linestyle', '-', 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
% placeholder for legend
plot(NaN, NaN, 'LineStyle', ':', 'LineWidth', 2.5, 'color', co(3, :), 'HandleVisibility','on');
%% concentration
% only integer xticks
xticks = get(gca, 'xtick');
xticks = xticks(mod(xticks, 1) == 0);
set(gca,'xtick', xticks);

set(gca,'GridLineStyle','--');
% primarily for tick label size
set(gca, 'FontSize', 12);
% xlabel('Number of Path', 'FontSize', 20);
ylabel('[X] (mole\cdotcm^{-3})', 'FontSize', 20);
% ylim([10^(1.0*log10(cumu_path_p_vec1(1))), 10^(0.9995*(log10(const_c1)))]);

yyaxis right;
error_data2 = (const_c1 - cumu_path_p_vec1)./const_c1.*100;
semilogy(path_idx(1:n2_delta1:end_path2_idx1), error_data2(1:n2_delta1:end_path2_idx1), ... 
    'LineStyle', ':', 'LineWidth', 2.5, 'color', co(3, :)); hold on;
% ylabel('%Relative Error', 'FontSize', 20);
% set(gca, 'ytick', []);

%% global settings
grid on;
xlim([1, end_path2_idx1]);

a_x = gca;
t_x = a_x.XLim(1) + 0.265*(a_x.XLim(2) - a_x.XLim(1));
t_y = a_x.YLim(1) + 0.858*(a_x.YLim(2) - a_x.YLim(1));
text(t_x, t_y, [spe_name, ', ', path_type2, ' pathways', ...
    newline 'time = ', num2str(str2double(end_t2)*str2double(tau), '%4.2f'), ' seconds'], ...
    'Fontsize', 14);

%% Zoom in figure2
co = [
    0   0   0
    0.4940    0.1840    0.5560 % 4th plot
];
set(fig,'defaultAxesColorOrder',co);
z2_position = [.475*x_ratio (.35+n_xaxis-iax)*y_ratio .255*x_ratio .25*y_ratio];
% create a new pair of axes inside current figure
axes('position',z2_position);

set(gca, 'ytick', []);
yyaxis right;
box on; % put box around new pair of axes
% pathway probability
semilogy(path_idx(1:n2_delta2:end_path2_idx2), path_p_vec1(1:n2_delta2:end_path2_idx2) , ...
    'color',co(2, :), 'linestyle', '-.', 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
tick_pos = linspace(ceil(log10(path_p_vec1(end_path2_idx2))),floor(log10(path_p_vec1(1))), ...
    1+floor(log10(path_p_vec1(1))) - ceil(log10(path_p_vec1(end_path2_idx2))));
tick_pos = arrayfun(@(x) 10^x, tick_pos);
set(gca, 'ytick', tick_pos);
axis tight;
grid on;
% legend
leg_z2 = legend(['PATHWAY' newline 'PROBABILITY']);
set(leg_z2, 'FontSize', 8, 'Box', 'off');
% [left, bottom, weight, height]
set(leg_z2, 'Position', [z2_position(1) + z2_position(3)*0.375, ...
    z2_position(2) + z2_position(4)*0.5, 0.1*x_ratio, 0.1*y_ratio]);

%##########################################################################
% Panel 3
%##########################################################################
iax = 3; % Or whichever
x0=xpos(1); y0=ypos((length(ypos)/2 - iax)*2 + 1); spanx=xpos(2) - xpos(1); spany=ypos((length(ypos)/2 - iax)*2 + 1 + 1) - ypos((length(ypos)/2 - iax)*2 + 1);
%% [left bottom width height]
pos = [x0 y0 spanx spany];
subplot('Position',pos);

%% sorted pathway probabilities
fn_path3_p1 = fullfile(file_dir, folder3, strcat(species_path3, 'path_prob_top_n_sorted_', ... 
    n_path3, '_',num2str(spe_idx), '_', tau, '_', end_t3, '.csv'));

delimiter = '';
formatSpec = '%f%[^\n\r]';
%% Open the text file.
fileID = fopen(fn_path3_p1,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter,  'ReturnOnError', false);
%% Close the text file.
fclose(fileID);
path_p_vec1 = dataArray{:, 1};
%% Clear temporary variables
clearvars fn_path_p1 delimiter formatSpec fileID dataArray ans;

% concentration of a species
const_c1 = interp1(time_vec, conc_mat(:, spe_idx + 1), str2double(end_t3) * str2double(tau));

%% convert pathway probability to concentration
path_p_2_X_vec1 = path_p_vec1 * pre_factor;

%% cumulative pathway probabilities
cumu_path_p_vec1 = path_p_2_X_vec1;
for i = 2:length(cumu_path_p_vec1)
    cumu_path_p_vec1(i) = cumu_path_p_vec1(i) + cumu_path_p_vec1(i-1);
end

%% linear array
path_idx = linspace(1, length(path_p_2_X_vec1), length(path_p_2_X_vec1));
path_idx = path_idx';

%% plot
% https://www.mathworks.com/help/matlab/graphics_transition/why-are-plot-lines-different-colors.html
% https://www.mathworks.com/help/matlab/creating_plots/customize-graph-with-two-y-axes.html
co = [    0    0.4470    0.7410 % 1th plot
%     0.8500    0.3250    0.0980 % 2nd plot
    0.9290    0.6940    0.1250 % 3rd plot
%     0.4940    0.1840    0.5560 % 4th plot
%     0.4660    0.6740    0.1880 % 5th plot
%     0.3010    0.7450    0.9330 % 6th plot
%     0.6350    0.0780    0.1840 % 7th plot
%     0         0    1.0000 % 8th plot, blue
    1   0   0 % for placeholder
    1   0   0]; % 9th plot, red
set(fig,'defaultAxesColorOrder',co)

% plot pathway probabilities
% % individual
% plot([path_idx(1), path_idx(end_path3_idx1)], [const_c1, const_c1], ...
%     'color',co(1, :), 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
% cumulative
plot(path_idx(1:n3_delta1:end_path3_idx1), cumu_path_p_vec1(1:n3_delta1:end_path3_idx1) , ...
    'color',co(2, :), 'linestyle', '-', 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
% placeholder for legend
plot(NaN, NaN, 'LineStyle', ':', 'LineWidth', 2.5, 'color', co(3, :), 'HandleVisibility','on');
%% concentration
% only integer xticks
xticks = get(gca, 'xtick');
xticks = xticks(mod(xticks, 1) == 0);
set(gca,'xtick', xticks);

set(gca,'GridLineStyle','--');
% primarily for tick label size
set(gca, 'FontSize', 12);
xlabel('Number of Path', 'FontSize', 20);
ylabel('[X] (mole\cdotcm^{-3})', 'FontSize', 20);
% ylim([10^(1.0*log10(cumu_path_p_vec1(1))), 10^(0.9995*(log10(const_c1)))]);

yyaxis right;
error_data3 = (const_c1 - cumu_path_p_vec1)./const_c1.*100;
semilogy(path_idx(1:n3_delta1:end_path3_idx1), error_data3(1:n3_delta1:end_path3_idx1), ... 
    'LineStyle', ':', 'LineWidth', 2.5, 'color', co(3, :)); hold on;
% ylabel('%Relative Error', 'FontSize', 20);
% set(gca, 'ytick', []);

%% global settings
grid on;
xlim([1, end_path3_idx1]);

a_x = gca;
t_x = a_x.XLim(1) + 0.265*(a_x.XLim(2) - a_x.XLim(1));
t_y = a_x.YLim(1) + 0.728*(a_x.YLim(2) - a_x.YLim(1));
text(t_x, t_y, [spe_name, ', ', path_type3, ' pathways', ...
    newline 'time = ', num2str(str2double(end_t3)*str2double(tau), '%4.2f'), ' seconds'], ...
    'Fontsize', 14);

%% Zoom in figure2
co = [
    0   0   0
    0.4940    0.1840    0.5560 % 4th plot
];
set(fig,'defaultAxesColorOrder',co);
z2_position = [.505*x_ratio (.475+n_xaxis-iax)*y_ratio .255*x_ratio .25*y_ratio];
% create a new pair of axes inside current figure
axes('position',z2_position);

set(gca, 'ytick', []);
yyaxis right;
box on; % put box around new pair of axes
% pathway probability
semilogy(path_idx(1:n3_delta2:end_path3_idx2), path_p_vec1(1:n3_delta2:end_path3_idx2) , ...
    'color',co(2, :), 'linestyle', '-.', 'LineWidth', 2.5, 'HandleVisibility','on'); hold on;
tick_pos = linspace(ceil(log10(path_p_vec1(end_path3_idx2))),floor(log10(path_p_vec1(1))), ...
    1+floor(log10(path_p_vec1(1))) - ceil(log10(path_p_vec1(end_path3_idx2))));
tick_pos = arrayfun(@(x) 10^x, tick_pos);
set(gca, 'ytick', tick_pos);
axis tight;
grid on;
% legend
leg_z2 = legend(['PATHWAY' newline 'PROBABILITY']);
set(leg_z2, 'FontSize', 8, 'Box', 'off');
% [left, bottom, weight, height]
set(leg_z2, 'Position', [z2_position(1) + z2_position(3)*0.375, ...
    z2_position(2) + z2_position(4)*0.5, 0.1*x_ratio, 0.1*y_ratio]);

%##########################################################################
% Figure size
%##########################################################################
x0=10;
y0=10;
width=400;
height=900;
set(gcf,'units','points','position',[x0,y0,width,height]);

%% save to file
figname = strcat(species_path1, 'pathway_prob_concentration_3in1_',num2str(spe_idx), '_v3.png');
print(fig, fullfile(file_dir, figname), '-r200', '-dpng');